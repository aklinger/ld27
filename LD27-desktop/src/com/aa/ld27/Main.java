package com.aa.ld27;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.imagepacker.TexturePacker2;

public class Main {
	private static final String ATLAS_INPUT_DIR = "../ld27-android/assets/gfx";
    private static final String ATLAS_OUTPUT_DIR = "../ld27-android/assets/image-atlases";
    private static final String ATLAS_PACK_FILE = "pages-info";
    
    private static final boolean rePackAtlas = true;
    
	public static void main(String[] args) {
		if(rePackAtlas){
			try{
				TexturePacker2.process(ATLAS_INPUT_DIR, ATLAS_OUTPUT_DIR, ATLAS_PACK_FILE);
			}catch (Exception ex){
				//LogUtil.error("Couldn't repack atlas, totally okay if it still runs.", ex);
			}
		}
		
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "LD27";
		cfg.useGL20 = false;
		cfg.width = 800;
		cfg.height = 600;
		
		new LwjglApplication(new GameLoop(), cfg);
	}
}
