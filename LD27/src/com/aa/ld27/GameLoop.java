package com.aa.ld27;

import com.aa.ld27.gui.SplashScreen;
import com.aa.ld27.util.LogUtil;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.FPSLogger;

public class GameLoop extends Game {
	//Attribute
    private FPSLogger fpsLogger;
    private static GameLoop instance;
    //Methoden
    public static void setCurrentScreen(Screen screen){
        instance.setScreen(screen);
    }
    @Override
    public void create() {
        LogUtil.log("Game Created");
        fpsLogger = new FPSLogger();
        setScreen(new SplashScreen());
        instance = this;
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        LogUtil.log("Game Resized");
    }

    @Override
    public void render() {
        super.render();

        fpsLogger.log();
    }

    @Override
    public void pause() {
        super.pause();
        LogUtil.log("Game Paused");
    }

    @Override
    public void resume() {
        super.resume();
        LogUtil.log("Game Resumed");
    }

    @Override
    public void dispose() {
        super.dispose();
        LogUtil.log("Game Disposed");
    }
}
