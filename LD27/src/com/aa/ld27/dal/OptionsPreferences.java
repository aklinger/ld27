package com.aa.ld27.dal;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;

/**
 * Saves the configurations of the options to Preferences.
 * 
 * @author Toni
 */
public class OptionsPreferences {
    //Attributes
    private static final String PREF_VOLUME = "volume";
    private static final String PREF_MUSIC_ENABLED = "music.enabled";
    private static final String PREF_SOUND_ENABLED = "sound.enabled";
    private static final String PREFS_NAME = "ld27warmup";
    
    private static OptionsPreferences instance;
    
    private Preferences prefs;
    //Constructors
    public static OptionsPreferences getInstance(){
        if(instance == null){
            instance = new OptionsPreferences();
        }
        return instance;
    }
    private OptionsPreferences(){
        prefs = Gdx.app.getPreferences(PREFS_NAME);
    }
    //Methods
    public boolean isSoundEffectsEnabled()
    {
        return prefs.getBoolean( PREF_SOUND_ENABLED, true );
    }
 
    public void setSoundEffectsEnabled(
        boolean soundEffectsEnabled )
    {
        prefs.putBoolean( PREF_SOUND_ENABLED, soundEffectsEnabled );
        prefs.flush();
    }
 
    public boolean isMusicEnabled()
    {
        return prefs.getBoolean( PREF_MUSIC_ENABLED, false );
    }
 
    public void setMusicEnabled(
        boolean musicEnabled )
    {
        prefs.putBoolean( PREF_MUSIC_ENABLED, musicEnabled );
        prefs.flush();
    }
 
    public float getVolume()
    {
        return prefs.getFloat( PREF_VOLUME, 0.9f );
    }
 
    public void setVolume(
        float volume )
    {
        prefs.putFloat( PREF_VOLUME, volume );
        prefs.flush();
    }
    //Getter & Setter
    
}
