package com.aa.ld27.dal;

import com.aa.ld27.util.LogUtil;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;

import java.util.HashMap;
import java.util.Map;

/**
 * Manages a Cache of sound Effects.
 * 
 * @author Toni
 */
public class SoundManager {
    //Attributes
    private float volume = 1f;
    private boolean enabled = true;
    private Map<String,Sound> sounds;
    
    private static SoundManager instance;
    
    public static final String SFX_LASER_SHOOTWAV = "sfx/Laser_Shoot.wav";
    public static final String RUMBLE = "sfx/Rumble1.wav";
    public static final String HIT1 = "sfx/Hit_Hurt11.wav";
    public static final String HIT2 = "sfx/Hit_Hurt18.wav";
    public static final String LESSER_HIT = "sfx/Hit_Hurt6.wav";
    public static final String JUMP = "sfx/Jump6.wav";
    public static final String CLICK1 = "sfx/Blip_Select29.wav";
    public static final String CLICK2 = "sfx/Blip_Select30.wav";
    public static final String MEH = "sfx/meh.wav";
    public static final String EAT1 = "sfx/eat1.wav";
    public static final String EAT2 = "sfx/eat2.wav";
    public static final String DRAGON_FIRE = "sfx/dragon_fireball.mp3";
    public static final String DRAGON_ROAR = "sfx/dragon_roar.mp3";
    public static final String CRUMBLE = "sfx/crumble.mp3";
    
    public static final String[] MENU_BUTTON = new String[]{CLICK1,CLICK2};
    
    //Constructors
    public static SoundManager getInstance(){
        if(instance == null){
            instance = new SoundManager();
        }
        return instance;
    }
    public SoundManager() {
        sounds = new HashMap<String, Sound>();
    }
    //Methods
    public void play (String... sounds){
        int chosen = (int)(Math.random()*sounds.length);
        play(sounds[chosen]);
    }
    public void play (String sound){
        if( !enabled ) return;
        
        Sound soundToPlay = sounds.get(sound);
        if(soundToPlay == null){
            FileHandle soundFile = Gdx.files.internal(sound);
            soundToPlay = Gdx.audio.newSound(soundFile);
            sounds.put(sound,soundToPlay);
        }
        
        LogUtil.debug("Playing sound: "+ sound);
        
        soundToPlay.play(volume);          
    }
    public void dispose(){
        LogUtil.log("Disposing the sound manager");
        for(Sound sound : sounds.values()){
            sound.stop();
            sound.dispose();
        }
    }

    public void setVolume(float volume) {
        this.volume = volume;
    }
    
}
