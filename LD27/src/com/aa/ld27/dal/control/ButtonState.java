package com.aa.ld27.dal.control;

import com.badlogic.gdx.Gdx;

/**
 * Saves the State of one Keyboard Button.
 * 
 * @author Toni
 */
public class ButtonState implements InputButton{
    //Attributes
    private int key;
    private boolean down;
    private boolean pressed;
    private boolean released;
    
    //Constructors
    public ButtonState(int key) {
        this.key = key;
    }
    
    //Methods
    public void update(){
        pressed = false;
        released = false;
        if(Gdx.input.isKeyPressed(key)){
            if(!down){
                down = true;
                pressed = true;
            }
        }else{
            if(down){
                down = false;
                released = true;
            }
        }
    }
    
    //Getter & Setter
    @Override
    public boolean isDown() {
        return down;
    }
    @Override
    public boolean isPressed() {
        return pressed;
    }
    @Override
    public boolean isReleased() {
        return released;
    }
}
