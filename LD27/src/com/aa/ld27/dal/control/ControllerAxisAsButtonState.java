package com.aa.ld27.dal.control;

import com.badlogic.gdx.controllers.Controller;

/**
 * Makes an Axis of a Controller work as a button.
 * 
 * @author Toni
 */
public class ControllerAxisAsButtonState implements InputButton{
    //Attributes
    private Controller controller;
    private int axis;
    private float treshold;
    private boolean down;
    private boolean pressed;
    private boolean released;
    
    //Constructors
    public ControllerAxisAsButtonState(Controller controller, int axis, float treshold) {
    	this.controller = controller;
        this.axis = axis;
        this.treshold = treshold;
    }
    
    //Methods
    @Override
    public void update(){
        pressed = false;
        released = false;
        if(isButtonDown(controller)){
            if(!down){
                down = true;
                pressed = true;
            }
        }else{
            if(down){
                down = false;
                released = true;
            }
        }
    }
    
    private boolean isButtonDown(Controller controller){
        if(treshold>0){
            return controller.getAxis(axis)>treshold;
        }else{
            return controller.getAxis(axis)<treshold;
        }
    }
    
    //Getter & Setter
    @Override
    public boolean isDown() {
        return down;
    }
    @Override
    public boolean isPressed() {
        return pressed;
    }
    @Override
    public boolean isReleased() {
        return released;
    }
}
