package com.aa.ld27.dal.control;

import com.badlogic.gdx.controllers.Controller;

/**
 * Holds the State of a Controller Button.
 * 
 * @author Toni
 */
public class ControllerButtonState implements InputButton{
    //Attributes
    private Controller controller;
    private int key;
    private boolean down;
    private boolean pressed;
    private boolean released;
    
    //Constructors
    public ControllerButtonState(Controller controller, int key) {
    	this.controller = controller;
        this.key = key;
    }
    
    //Methods
    @Override
    public void update(){
        pressed = false;
        released = false;
        if(controller.getButton(key)){
            if(!down){
                down = true;
                pressed = true;
            }
        }else{
            if(down){
                down = false;
                released = true;
            }
        }
    }
    
    //Getter & Setter
    @Override
    public boolean isDown() {
        return down;
    }
    @Override
    public boolean isPressed() {
        return pressed;
    }
    @Override
    public boolean isReleased() {
        return released;
    }
}
