package com.aa.ld27.dal.control;

import java.util.ArrayList;
import java.util.List;


public class ControlsManager {
	private List<GoatControlScheme> steuerungen;
	
	private static ControlsManager instance;
	
	private ControlsManager(){
		steuerungen = new ArrayList<GoatControlScheme>();
	}
	
	public static ControlsManager getInstance(){
		if(instance == null){
			instance = new ControlsManager();
		}
		return instance;
	}
	
	public List<GoatControlScheme> getSteuerungen() {
		return steuerungen;
	}
}
