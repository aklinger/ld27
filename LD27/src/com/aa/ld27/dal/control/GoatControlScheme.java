package com.aa.ld27.dal.control;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;

public class GoatControlScheme {
    private InputButton left;
    private InputButton right;
    private InputButton up;
    private InputButton down;
    private InputButton jump;
    private InputButton run;
    private InputButton smash;
    private InputButton pause;
    private InputButton shit;
    private InputButton meh;
    private InputButton eat;

    public GoatControlScheme(Controller controller, int player) {
        if (player == 1) {
            left = new ButtonState(Input.Keys.LEFT);
            right = new ButtonState(Input.Keys.RIGHT);
            up = new ButtonState(Input.Keys.UP);
            down = new ButtonState(Input.Keys.DOWN);
            jump = new ButtonState(Input.Keys.UP);
            run = new ButtonState(Input.Keys.J);
            smash = new ButtonState(Input.Keys.DOWN);
            pause = new ButtonState(Input.Keys.BACKSPACE);
            shit = new ButtonState(Input.Keys.I);
            meh = new ButtonState(Input.Keys.K);
            eat = new ButtonState(Input.Keys.L);
        } else if(player == 0){
            left = new ButtonState(Input.Keys.A);
            right = new ButtonState(Input.Keys.D);
            up = new ButtonState(Input.Keys.W);
            down = new ButtonState(Input.Keys.S);
            jump = new ButtonState(Input.Keys.W);
            run = new ButtonState(Input.Keys.F);
            smash = new ButtonState(Input.Keys.S);
            pause = new ButtonState(Input.Keys.P);
            shit = new ButtonState(Input.Keys.T);
            meh = new ButtonState(Input.Keys.G);
            eat = new ButtonState(Input.Keys.H);
        }
        if(controller != null){
            left = new ControllerAxisAsButtonState(controller, 1, -0.1f);
            right = new ControllerAxisAsButtonState(controller, 1, 0.1f);
            up = new ControllerAxisAsButtonState(controller, 0, -0.1f);
            down = new ControllerAxisAsButtonState(controller, 0, 0.1f);
            jump = new ControllerButtonState(controller, 0);
            run = new ControllerButtonState(controller, 1);
            smash = new ControllerButtonState(controller, 2);
            pause = new ControllerButtonState(controller, 6);
            shit = new ControllerButtonState(controller, 3);
            meh = new ControllerButtonState(controller, 4);
            eat = new ControllerButtonState(controller, 5);
        }
    }
    
    public void update(){
        left.update();
        right.update();
        up.update();
        down.update();
        jump.update();
        run.update();
        smash.update();
        pause.update();
        shit.update();
        meh.update();
        eat.update();
    }

    public InputButton getLeft() {
        return left;
    }

    public InputButton getRight() {
        return right;
    }

    public InputButton getUp() {
        return up;
    }

    public InputButton getDown() {
        return down;
    }

    public InputButton getJump() {
        return jump;
    }

    public InputButton getRun() {
        return run;
    }

    public InputButton getSmash() {
        return smash;
    }

    public InputButton getPause() {
        return pause;
    }

    public InputButton getShit() {
        return shit;
    }

    public InputButton getMeh() {
        return meh;
    }

    public InputButton getEat() {
        return eat;
    }
    
}
