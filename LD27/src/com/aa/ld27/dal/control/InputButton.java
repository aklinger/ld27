package com.aa.ld27.dal.control;

/**
 * Defines what a Button can deliver for informations.
 * 
 * @author Toni
 */
public interface InputButton {
    //Methods
    public void update();
    public boolean isDown();
    public boolean isPressed();
    public boolean isReleased();
    
}
