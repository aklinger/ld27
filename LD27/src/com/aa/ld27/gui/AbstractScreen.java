package com.aa.ld27.gui;

import com.aa.ld27.dal.SoundManager;
import com.aa.ld27.util.LogUtil;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

/**
 * The base class for all game screens.
 * <p/>
 * Based on: http://steigert.blogspot.co.at/2012/02/3-libgdx-tutorial-scene2d.html
 * <p/>
 * @author Steigert
 */
public abstract class AbstractScreen implements Screen {
    protected final Stage stage;
    private Skin skin;
    private Table table;
    
    public boolean renderMenu;
    
    private static SpriteBatch batch;
    private static TextureAtlas atlas;
    private static SoundManager soundManager;

    public AbstractScreen() {
        this.stage = new Stage(0, 0, true, getBatch());
    }

    protected String getNameOfClass() {
        return "Screenname";//getClass().getSimpleName();
    }

    protected Skin getSkin() {
        if (skin == null) {
            skin = new Skin(Gdx.files.internal("gui/uiskin.json"));
        }
        return skin;
    }

    protected Table getTable() {
        if (table == null) {
            table = new Table(getSkin());
            table.setFillParent(true);
            stage.addActor(table);
        }
        return table;
    }
    
    public static SpriteBatch getBatch(){
    	if(batch == null){
    		batch = new SpriteBatch();
    	}
    	return batch;
    }

    public static TextureAtlas getAtlas() {
        if (atlas == null) {
            atlas = new TextureAtlas(Gdx.files.internal("image-atlases/pages-info.atlas"));
        }
        return atlas;
    }
    
    protected SoundManager getSoundManager(){
        if(soundManager == null){
            soundManager = SoundManager.getInstance();
        }
        return soundManager;
    }

    // Screen implementation
    @Override
    public void show() {
        LogUtil.log("Showing screen: " + getNameOfClass());

        Gdx.input.setInputProcessor(stage);
    }

    @Override
    public void resize(int width, int height) {
        LogUtil.log( "Resizing screen: " + getNameOfClass() + " to: " + width + " x " + height);

        // resize the stage
        stage.setViewport(width, height, true);
    }

    @Override
    public void render(float delta) {
        // the following code clears the screen with the given RGB color (black)
        Gdx.gl.glClearColor(0.1f, 0.2f, 0.5f, 1f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        // update and draw the stage actors
        stage.act(delta);
        if(!renderMenu){
            stage.draw();
        }
    }

    @Override
    public void hide() {
        LogUtil.log("Hiding screen: " + getNameOfClass());
    }

    @Override
    public void pause() {
        LogUtil.log("Pausing screen: " + getNameOfClass());
    }

    @Override
    public void resume() {
        LogUtil.log("Resuming screen: " + getNameOfClass());
    }

    @Override
    public void dispose() {
        LogUtil.log("Disposing screen: " + getNameOfClass());

        // dispose the collaborators
        stage.dispose();
        //batch.dispose();
    }
}
