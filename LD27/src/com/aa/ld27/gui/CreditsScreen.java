package com.aa.ld27.gui;

import com.aa.ld27.dal.SoundManager;
import com.aa.ld27.GameLoop;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 *
 * @author Toni
 */
public class CreditsScreen extends AbstractScreen {
    //Attribute
    //Konstruktor
    //Methoden
    @Override
    public void show() {
        super.show();

        // retrieve the default table actor
        Table table = super.getTable();
        table.defaults().spaceBottom(30);
        table.add("Credits").colspan(2);

        table.row();
        table.add("Programming:");
        table.add("");
        table.add("Anton Klinger");
        
        table.row();
        table.add("Artwork & Sound:");
        table.add("");
        table.add("Horst Petschenig");
        table.add("");
        table.add("Florian Kager");
        table.row();
        table.add("Special Thanks:");
        table.add("");
        table.add("Vinzenz");
        table.add("Raphi");
        table.add("Sgarzi");

        // register the back button
        TextButton backButton = new TextButton("Back to main menu", getSkin());
        backButton.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                getSoundManager().play(SoundManager.MENU_BUTTON);
                GameLoop.setCurrentScreen(new MenuScreen());
            }
        });
        table.row();
        table.add(backButton).size(250, 60).colspan(2);
    }
    @Override
    public void render(float delta) {
        super.render(delta);
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            GameLoop.setCurrentScreen(new MenuScreen());
        }
    }
}
