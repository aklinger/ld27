package com.aa.ld27.gui;

import com.aa.ld27.GameLoop;
import com.aa.ld27.dal.MusicManager;
import com.aa.ld27.dal.SoundManager;
import com.aa.ld27.dal.control.GoatControlScheme;
import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.map.GoatMap;
import com.aa.ld27.stuff.Door;
import com.aa.ld27.stuff.Entity;
import com.aa.ld27.stuff.GoatEntity;
import com.aa.ld27.stuff.Ground;
import com.aa.ld27.stuff.PowerupShittingPig;
import com.aa.ld27.stuff.Wolke;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.controllers.Controller;
import com.badlogic.gdx.controllers.Controllers;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

/**
 * The screen initalising the Game.
 * 
 * @author Toni
 */
public class GameScreen extends AbstractScreen {
    //Attribute
    private EntityManager manager;
    private int players;
    private boolean gameOver;
    
    //Konstruktor
    public GameScreen(int players) {
    	this.players = players;
    }
    //Methoden
    @Override
    public void show() {
        super.show();
        boolean enableControllers = players > 1;
        MusicManager.getInstance().play(MusicManager.GoatMusic.LEVEL);
        GoatMap map = GoatMap.createPlainMap(32, 8, 3, 64);
        manager = new EntityManager(map,this);
        for (int i = 0; i < players; i++) {
            GoatEntity player = new GoatEntity(new GoatControlScheme(null,i));
            manager.addPlayer(player);
            player.setX(200+100*i);
            player.setY(200);
            manager.addEntity(player);
        }
        if(enableControllers){
            Array<Controller> controllers = Controllers.getControllers();
            for (int i = 0; i < controllers.size; i++) {
                Controller c = controllers.get(i);
                GoatEntity player = new GoatEntity(new GoatControlScheme(c,i));
                manager.addPlayer(player);
                player.setX(200+100*i);
                player.setY(200+100);
                manager.addEntity(player);
            }
        }
        Ground ground = new Ground(80,368,true);
        ground.setY(100);
        manager.addEntity(ground);
        manager.addCastlePart(ground);
        ground = new Ground(160,100,true);
        ground.setY(180);
        manager.addEntity(ground);
        manager.addCastlePart(ground);
        manager.addEntity(new Ground(2048,192,false));
        Entity door = new Door(140,192,21,60,"powerup");
        manager.addEntity(door);
        manager.addCastlePart(door);
        
        manager.addEntity(new Wolke((float)(Math.random()*1536),570));
        for (int i = 0; i < 25; i++) {
            manager.addEntity(new Wolke((float)(Math.random()*1536),570+(float)(Math.random()*420)));
        }
        manager.addEntity(new PowerupShittingPig());
        //Array<Controller> controllers = Controllers.getControllers();
        /*for (int i = 0; i < 10; i++) {
            Entity ent = new BasicEnemy();
            ent.setX((int)(Math.random()*Gdx.graphics.getWidth()));
            ent.setY((int)(Math.random()*Gdx.graphics.getHeight()));
            manager.addEntity(ent);
        }
        for (int i = 0; i < 10; i++) {
            Entity ent = new FlyingEnemy();
            ent.setX((int)(Math.random()*Gdx.graphics.getWidth()));
            ent.setY((int)(Math.random()*Gdx.graphics.getHeight()));
            manager.addEntity(ent);
        }*/
    }

    @Override
    public void render(float delta) {
        super.render(delta);

        manager.act();
        manager.render();
        
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            GameLoop.setCurrentScreen(new MenuScreen());
        }
        if(Gdx.input.isKeyPressed(Input.Keys.M)){
        	MusicManager.getInstance().stop();
        }
    }
    
    //Getter & Setter
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        manager.resize();
    }
    
    public void showLoseScreen(boolean goats){
        if(!gameOver){
            gameOver = true;
            // retrieve the default table actor
            Table table = super.getTable();
            table.defaults().spaceBottom(30);
            table.add("GAME OVER!").colspan(3);

            if(goats){
                table.row();
                table.add("");
                table.add("You got killed.");
            }else{
                SoundManager.getInstance().play(SoundManager.CRUMBLE);
                table.row();
                table.add("");
                table.add("Your tower got destroyed.");
                table.row();
                table.add("");
                table.add("Where will you live now?");
            }

            // register the back button
            TextButton backButton = new TextButton("Back to main menu", getSkin());
            backButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                getSoundManager().play(SoundManager.MENU_BUTTON);
                    GameLoop.setCurrentScreen(new MenuScreen());
                }
            });
            table.row();
            table.add(backButton).size(250, 60).colspan(3);
        }
    }
    
    public void showVictoryScreen(){
        if(!gameOver){
            gameOver = true;
            // retrieve the default table actor
            Table table = super.getTable();
            table.defaults().spaceBottom(30);
            table.add("YOU WIN!").colspan(3);

            table.row();
            table.add("");
            table.add("You Successfully defended your tower!");
            table.row();
            table.add("");
            if(manager.getPlayers().size() > 1){
            	table.add("You are some awesome goats.");
            }else{
            	table.add("You are an awesome goat.");
        	}
            // register the back button
            TextButton backButton = new TextButton("The End", getSkin());
            backButton.addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                getSoundManager().play(SoundManager.MENU_BUTTON);
                    GameLoop.setCurrentScreen(new SplashScreen("gfx/Ekeby-Goats2.png",256,512));
                }
            });
            table.row();
            table.add(backButton).size(250, 60).colspan(3);
        }
    }
}
