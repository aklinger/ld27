package com.aa.ld27.gui;

import com.aa.ld27.GameLoop;
import com.aa.ld27.dal.SoundManager;
import com.aa.ld27.util.LogUtil;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * Pretty Self-Explanatory...
 * 
 * @author Toni
 */
public class LevelSelectionScreen extends AbstractScreen{
    //Attributes
    private TextButton player1Button;
    private TextButton player2Button;

    private LevelClickListener startClickListener;
    
    //Constructors
    public LevelSelectionScreen()
    {
        // create the listeners
        startClickListener = new LevelClickListener();
    }

    //Methods
    @Override
    public void show()
    {
        super.show();


        // start playing the menu music (the player might be returning from the
        // level screen)
        //game.getMusicManager().play( TyrianMusic.MENU );


        // retrieve the default table actor
        Table table = super.getTable();
        table.defaults().spaceBottom( 20 );
        table.columnDefaults( 0 ).padRight( 20 );
        table.columnDefaults( 4 ).padLeft( 10 );
        table.add( "Start Game" ).colspan( 5 );


        // retrieve the table's layout
        //profile = ProfileManager.getInstance().getCurrentProfile();
        //ship = profile.getShip();


        // create the level buttons
        table.row();
        table.add( "Players: " );


        player1Button = new TextButton( "1 Player", getSkin() );
        player1Button.addListener( startClickListener );
        table.add( player1Button ).fillX().padRight( 10 );


        player2Button = new TextButton( "2 Player", getSkin() );
        player2Button.addListener( startClickListener );
        table.add( player2Button ).fillX().padRight( 10 );


        // create the credits label
        table.row();
        table.add( "Player 1 Main Controls: " );
        table.add( "W, A, S, D and F" ).left().colspan( 2 );
        table.row();
        table.add( "For goat authenticity: " );
        table.add( "T, G and H" ).left().colspan( 2 );
        table.row();
        table.add( " " );
        table.row();
        table.add( "Player 2 Main Controls: " );
        table.add( "ArrowKeys and J" ).left().colspan( 2 );
        table.row();
        table.add( "For goat authenticity: " );
        table.add( "I, K and L" ).left().colspan( 2 );


        // register the back button
        TextButton backButton = new TextButton( "Back to main menu", getSkin() );
        backButton.addListener( new ClickListener() {
            @Override
            public void touchUp(
                InputEvent event,
                float x,
                float y,
                int pointer,
                int button )
            {
                getSoundManager().play(SoundManager.MENU_BUTTON);
            	GameLoop.setCurrentScreen( new MenuScreen() );
            }
        } );
        table.row();
        table.add( backButton ).size( 250, 60 ).colspan( 5 );

    }


    /**
     * Listener for all the level buttons.
     */
    private class LevelClickListener
        extends
            ClickListener
    {
        @Override
        public void touchUp(
            InputEvent event,
            float x,
            float y,
            int pointer,
            int button )
        {
            super.touchUp( event, x, y, pointer, button );
            getSoundManager().play(SoundManager.MEH);
            // find the target level ID
            int targetLevelId = - 1;
            Actor actor = event.getListenerActor();
            if( actor == player1Button ) {
                GameLoop.setCurrentScreen( new GameScreen(1));
            } else if( actor == player2Button ) {
                GameLoop.setCurrentScreen( new GameScreen(2));
            }
            LogUtil.log("SELECTED LEVEL: "+targetLevelId);
        }
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            GameLoop.setCurrentScreen(new MenuScreen());
        }
    }
}
