package com.aa.ld27.gui;

import com.aa.ld27.GameLoop;
import com.aa.ld27.dal.MusicManager;
import com.aa.ld27.dal.SoundManager;
import com.aa.ld27.miniLD45.LogicScreen;
import com.aa.ld27.util.LogUtil;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;


/**
 *
 * @author Toni
 */
public class MenuScreen extends AbstractScreen {
    //Attribute
    private int escapeTime = 0;
    private int escapeDuration = 60;
    //Konstruktor
    
    //Methoden
    @Override
    public void show(){
        super.show();
        // register the button "start game"
        MusicManager.getInstance().stop();
        TextButton startGameButton = new TextButton( "Start game", getSkin() );
        startGameButton.addListener( new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                LogUtil.log("STARTGAMEBUTTON");
                getSoundManager().play(SoundManager.MENU_BUTTON);
                GameLoop.setCurrentScreen(new LevelSelectionScreen());
            }
        } );


        // register the button "options"
        TextButton optionsButton = new TextButton( "Options", getSkin() );
        optionsButton.addListener( new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                LogUtil.log("OPTIONS_BUTTON");
                getSoundManager().play(SoundManager.MENU_BUTTON);
                GameLoop.setCurrentScreen(new OptionsScreen());
            }
        } );


        // register the button "high scores"
        TextButton highScoresButton = new TextButton( "Credits", getSkin() );
        highScoresButton.addListener( new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                LogUtil.log("HIGHSCORES_BUTTON");
                getSoundManager().play(SoundManager.MENU_BUTTON);
                GameLoop.setCurrentScreen(new CreditsScreen());
            }
        } );
        
        // register the button "high scores"
        TextButton bonusButton = new TextButton( "[Mini Logic Puzzle Prototype]", getSkin() );
        bonusButton.addListener( new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                LogUtil.log("Bonus_BUTTON");
                getSoundManager().play(SoundManager.MENU_BUTTON);
                GameLoop.setCurrentScreen(new LogicScreen());
            }
        } );
        
        // creates the table actor
        Table table = super.getTable();
        // add the welcome message with a margin-bottom of 50 units
        table.add( "Welcome to the goat tower!" ).spaceBottom( 50 );
        // move to the next row
        table.row();
        // add the start-game button sized 300x60 with a margin-bottom of 10 units
        table.add( startGameButton ).size( 300f, 60f ).uniform().spaceBottom( 10 );
        // move to the next row
        table.row();
        // add the options button in a cell similar to the start-game button's cell
        table.add( optionsButton ).uniform().fill().spaceBottom( 10 );
        // move to the next row
        table.row();
        // add the high-scores button in a cell similar to the start-game button's cell
        table.add( highScoresButton ).uniform().fill().spaceBottom( 30 );
        // move to the next row
        table.row();
        table.add( bonusButton ).uniform().fill();
    }
    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            escapeTime++;
        }else{
            escapeTime = 0;
        }
        if(escapeTime>escapeDuration){
            Gdx.app.exit();
        }
    }
}
