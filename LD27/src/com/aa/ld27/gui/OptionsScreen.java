package com.aa.ld27.gui;

import com.aa.ld27.GameLoop;
import com.aa.ld27.dal.MusicManager;
import com.aa.ld27.dal.OptionsPreferences;
import com.aa.ld27.dal.SoundManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

/**
 * The Options-screen showing the options for sound.
 * <p/>
 * @author Toni
 */
public class OptionsScreen extends AbstractScreen {
    //Attribute
    private Label volumeValue;

    //Methoden
    @Override
    public void show() {
        super.show();

        // retrieve the default table actor
        Table table = super.getTable();
        table.defaults().spaceBottom(30);
        table.columnDefaults(0).padRight(20);
        table.add("Options").colspan(3);


        // create the labels widgets
        /*final CheckBox soundEffectsCheckbox = new CheckBox("", getSkin());
        soundEffectsCheckbox.setChecked(OptionsPreferences.getInstance().isSoundEffectsEnabled());
        soundEffectsCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(
                    ChangeEvent event,
                    Actor actor) {
                boolean enabled = soundEffectsCheckbox.isChecked();
                OptionsPreferences.getInstance().setSoundEffectsEnabled(enabled);
                getSoundManager().play(SoundManager.CLICK1,SoundManager.CLICK2,SoundManager.CLICK3);
            }
        });
        table.row();
        table.add("Sound Effects");
        table.add(soundEffectsCheckbox).colspan(2).left();*/


        final CheckBox musicCheckbox = new CheckBox("", getSkin());
        musicCheckbox.setChecked(OptionsPreferences.getInstance().isMusicEnabled());
        musicCheckbox.addListener(new ChangeListener() {
            @Override
            public void changed(
                    ChangeEvent event,
                    Actor actor) {
                boolean enabled = musicCheckbox.isChecked();
                OptionsPreferences.getInstance().setMusicEnabled(enabled);
                MusicManager.getInstance().setEnabled(enabled);
                getSoundManager().play(SoundManager.MENU_BUTTON);
            }
        });
        table.row();
        table.add("Music");
        table.add(musicCheckbox).colspan(2).left();


        // range is [0.0,1.0]; step is 0.1f
        Slider volumeSlider = new Slider(0f, 1f, 0.1f, false, getSkin());
        volumeSlider.setValue( OptionsPreferences.getInstance().getVolume() );
        volumeSlider.addListener(new ChangeListener() {
            @Override
            public void changed(
                    ChangeEvent event,
                    Actor actor) {
                float value = ((Slider) actor).getValue();
                OptionsPreferences.getInstance().setVolume(value);
                MusicManager.getInstance().setVolume( value );
                SoundManager.getInstance().setVolume( value );
                updateVolumeLabel();
            }
        });


        // create the volume label
        volumeValue = new Label("", getSkin());
        updateVolumeLabel();


        // add the volume row
        table.row();
        table.add("Volume");
        table.add(volumeSlider);
        table.add(volumeValue).width(40);


        // register the back button
        TextButton backButton = new TextButton("Back to main menu", getSkin());
        backButton.addListener(new ClickListener() {
            @Override
            public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
                super.touchUp(event, x, y, pointer, button);
                getSoundManager().play(SoundManager.MENU_BUTTON);
                GameLoop.setCurrentScreen(new MenuScreen());
            }
        });
        table.row();
        table.add(backButton).size(250, 60).colspan(3);
    }

    /**
     * Updates the volume label next to the slider.
     */
    private void updateVolumeLabel() {
        float volume = (OptionsPreferences.getInstance().getVolume() * 100);
        volumeValue.setText(""+(Math.round(volume*100)/100)+" %");
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            GameLoop.setCurrentScreen(new MenuScreen());
        }
    }
}
