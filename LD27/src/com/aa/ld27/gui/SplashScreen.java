package com.aa.ld27.gui;

import com.aa.ld27.GameLoop;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Align;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Scaling;

/**
 * A splash-Screen showing at the beginning of the game.
 * <p/>
 * @author Toni
 */
public class SplashScreen extends AbstractScreen {
    //Attribute
    private Texture splashTexture;
    private Image splashImage;
    private String image;
    private int width, height;
    
    //Konstruktor
    public SplashScreen() {
        image = "gfx/title.png";
        width = 64;
        height = 32;
    }

    public SplashScreen(String image, int width, int height) {
        this.image = image;
        this.width = width;
        this.height = height;
    }
    
    
    //Methoden
    @Override
    public void show() {
        splashTexture = new Texture(image);

        splashTexture.setFilter(TextureFilter.Nearest, TextureFilter.Nearest);

        stage.clear();

        TextureRegion splashRegion = new TextureRegion(splashTexture, 0, 0, width, height);

        splashImage = new Image(new TextureRegionDrawable(splashRegion), Scaling.stretch, Align.bottom | Align.left);

        splashImage.setColor(1, 1, 1, 0);

        splashImage.addAction(Actions.sequence(Actions.fadeIn(0.15f), Actions.delay(2f), Actions.fadeOut(0.15f),
                new Action() {
            @Override
            public boolean act(float delta) {
            	GameLoop.setCurrentScreen(new MenuScreen());
                return true; // returning true consumes the event
            }
        }));
        // and finally we add the actors to the stage, on the correct order
        stage.addActor(splashImage);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);

        splashImage.setWidth(width);
        splashImage.setHeight(height);

        splashImage.invalidateHierarchy();
    }

    @Override
    public void render(float delta) {
        super.render(delta);
        
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            GameLoop.setCurrentScreen(new MenuScreen());
        }
    }
}
