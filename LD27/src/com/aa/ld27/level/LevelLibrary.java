package com.aa.ld27.level;

import com.aa.ld27.stuff.EnemyFactory;

/**
 * This stores the waves statically for now.
 * 
 * @author Toni
 */
public class LevelLibrary {
    //Attributes
    
    //Constructors
    
    //Methods
    public static Wave getWave(int levelID, int difficulty){
        Wave wave = new Wave();
        WaveDefinition def;
        switch(levelID){
            case 0:
                def = new WaveDefinition(EnemyFactory.NORMAL, 5 * difficulty, 80);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FLYING, 3 * difficulty, 100);
                wave.addSegment(def);
                break;
            case 1:
                def = new WaveDefinition(EnemyFactory.NORMAL, 1, 60*10);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.NORMAL, 4 * difficulty, 50);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FLYING, 2 * difficulty, 60);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.NORMAL, 4 * difficulty, 50);
                wave.addSegment(def);
                break;
            case 2:
                def = new WaveDefinition(EnemyFactory.NORMAL, 1, 60*10);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.NORMAL, 2 * difficulty, 60);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FROG, 2 * difficulty, 60);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.NORMAL, 2 * difficulty, 60);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FROG, 2 * difficulty, 60);
                wave.addSegment(def);
                break;
            case 3:
                def = new WaveDefinition(EnemyFactory.FROG, 1, 60*10);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FLYING, 2 * difficulty, 62);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FROG, 4 * difficulty, 64);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.NORMAL, 7 * difficulty, 62);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FROG, 5 * difficulty, 64);
                wave.addSegment(def);
                break;
            case 4:
                def = new WaveDefinition(EnemyFactory.FLYING, 1, 60*10);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FLYING, 10 * difficulty, 92);
                wave.addSegment(def);
                break;
            case 5:
                def = new WaveDefinition(EnemyFactory.FLYING, 1, 60*10);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.NORMAL, 1 * difficulty, 70);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FROG, 1 * difficulty, 70);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.DRAGON, 1 * difficulty, 700);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FROG, 1 * difficulty, 300);
                wave.addSegment(def);
                break;
            case 6:
                def = new WaveDefinition(EnemyFactory.FLYING, 1, 60*10);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FROG, 20 * difficulty, 100);
                wave.addSegment(def);
                def = new WaveDefinition(EnemyFactory.FLYING, 1 * difficulty, 60);
                wave.addSegment(def);
                break;
        }
        return wave;
    }
    
    //Getter & Setter
}
