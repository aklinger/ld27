package com.aa.ld27.level;

import com.aa.ld27.logic.EntityManager;
import java.util.ArrayList;
import java.util.List;

/**
 * A Wave from the Wave-System
 * <p/>
 * @author Toni
 */
public class Wave {
    //Attributes
    private List<WaveDefinition> waveSegments;
    private int timer;
    
    //Constructors
    public Wave() {
        waveSegments = new ArrayList<WaveDefinition>();
        timer = 0;
    }
    
    //Methods
    public void reset(){
        timer = 0;
    }
    
    public void addSegment(WaveDefinition segment){
        waveSegments.add(segment);
    }
    
    public void act(EntityManager manager){
        timer++;
        int count = 0;
        outer:
        for (int i = 0; i < waveSegments.size(); i++) {
            WaveDefinition seg = waveSegments.get(i);
            for (int j = 0; j < seg.getCount(); j++) {
                count += seg.getRate();
                if(count == timer){
                    manager.spawnEnemy(seg.getEnemyType());
                }else if(count > timer){
                    break outer;
                }
            }
        }
    }
    
    public WaveDefinition getCurrentWaveSegment(){
        int count = 0;
        for (int i = 0; i < waveSegments.size(); i++) {
            WaveDefinition seg = waveSegments.get(i);
            count += seg.getCount()*seg.getRate();
            if(count > timer){
                return seg;
            }
        }
        return null;
    }
    
    public boolean isFinished(){
        return getCurrentWaveSegment() == null;
    }
    
    //Getter & Setter
    
}
