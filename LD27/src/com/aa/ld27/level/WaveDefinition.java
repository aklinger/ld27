package com.aa.ld27.level;

/**
 * The Definition of part of the Wave.
 * 
 * @author Toni
 */
public class WaveDefinition {
    //Attributes
    private int enemyType;
    private int count;
    private int rate;
    
    //Constructors
    public WaveDefinition(int enemyType, int count, int rate) {
        this.enemyType = enemyType;
        this.count = count;
        this.rate = rate;
    }
    
    //Methods
    
    //Getter & Setter
    public int getEnemyType() {
        return enemyType;
    }

    public void setEnemyType(int enemyType) {
        this.enemyType = enemyType;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }
}
