package com.aa.ld27.logic;

import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.gui.GameScreen;
import com.aa.ld27.level.LevelLibrary;
import com.aa.ld27.level.Wave;
import com.aa.ld27.map.GoatMap;
import com.aa.ld27.maths.Collision;
import com.aa.ld27.maths.CollisionUtil;
import com.aa.ld27.particle.ParticleList;
import com.aa.ld27.particle.ParticleManager;
import com.aa.ld27.stuff.Atom;
import com.aa.ld27.stuff.BreakingCastle;
import com.aa.ld27.stuff.EnemyFactory;
import com.aa.ld27.stuff.Entity;
import static com.aa.ld27.stuff.Entity.resolveCollision;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

/**
 * Manages all entities that are in the game at any given moment.
 * 
 * @author Toni
 */
public class EntityManager {
    //Attribute
    private List<Entity> entities;
    private List<Entity> inbox;
    private List<Entity> trash;
    private ParticleManager particles;
    
    private List<Atom> atoms;
    
    private List<Entity> players;
    
    private List<HealthBar> healthBars;
    
    private GoatMap map;
    private OrthographicCamera cam;
    private Wave currentWave;
    private int currentLevel;
    
    private int levelSize = 1536;
    
    private BreakingCastle castleEntity;
    private HealthBar castleHealthBar;
    private List<Entity> castleParts;
    
    private GameScreen screen;
    
    //Konstruktor
    public EntityManager(GoatMap map, GameScreen screen) {
        this.screen = screen;
        
        entities = new ArrayList<Entity>();
        inbox = new ArrayList<Entity>();
        trash = new ArrayList<Entity>();
        particles = new ParticleList();
        
        this.map = map;
        currentWave = LevelLibrary.getWave(currentLevel, 1);
        cam = new OrthographicCamera(Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.update();
        healthBars = new ArrayList<HealthBar>();
        players = new ArrayList<Entity>();
        
        castleEntity = new BreakingCastle(0,192,200,400);
        entities.add(castleEntity);
        castleEntity.setMaxHealth(14*2);//CastleLife
        castleHealthBar = new HealthBar(10, Gdx.graphics.getHeight()-10-8*8, "TowerBar/tower-health",4);
        castleHealthBar.setParent(castleEntity);
        castleParts = new ArrayList<Entity>();
        resize();
        fillScreenWithAtoms();
    }
    
    private void fillScreenWithAtoms(){
        atoms = new ArrayList<>();
        for (int i = 0; i < 100; i++) {
            Atom a = new Atom();
            int range = 100;
            a.setX((int)(Math.random()*range));
            a.setY(300+(int)(Math.random()*range));
            float speed = 5;
            a.setSx((float)(speed*Math.random())-speed/2);
            a.setSy((float)(speed*Math.random())-speed/2);
            atoms.add(a);
        }
    }
    
    public void addAtom(Atom a){
        atoms.add(a);
    }
    
    //Methoden
    public boolean allEnemiesDead(){
        for (Entity entity : entities) {
            if(entity.isHostile()){
                return false;
            }
        }
        return true;
    }
    public void act(){
        if(!inbox.isEmpty()){
            entities.addAll(inbox);
            inbox.clear();
        }
        currentWave.act(this);
        if(currentWave.isFinished()){
        	currentLevel++;
        	currentWave = LevelLibrary.getWave(currentLevel, players.size());
                if(currentLevel >=8 && allEnemiesDead()){
                    screen.showVictoryScreen();
                }
        }
        for (int i = 0; i < entities.size(); i++) {
            Entity ent1 = entities.get(i);
            for (int j = i+1; j < entities.size(); j++) {
                Entity ent2 = entities.get(j);
                Collision col = CollisionUtil.calcCollisionHitBoxes(ent1, ent2, true, false);
                if(ent1.collidesWith(ent2) && ent2.collidesWith(ent1) && col != null){
                    resolveCollision(col, !(ent1.isSpawnProtection() || ent2.isSpawnProtection()));
                }
                ent1.interactWith(ent2,col,this);
                ent2.interactWith(ent1,col,this);
            }
            ent1.act(this);
            if(!ent1.isAlive()){
                ent1.die(this);
            	trash.add(ent1);
            }
        }
        for (int i = 0; i < entities.size(); i++) {
            Entity ent1 = entities.get(i);
            ent1.afterAct(this);
        }
        for (int i = 0; i < atoms.size(); i++) {
            Atom a1 = atoms.get(i);
            for (int j = i + 1; j < atoms.size(); j++) {
                Atom a2 = atoms.get(j);
                Collision col = CollisionUtil.calcCollisionHitBoxes(a1, a2, true, false);
                if(a1.collidesWith(a2) && a2.collidesWith(a1) && col != null){
                    resolveCollision(col, !(a1.isSpawnProtection() || a2.isSpawnProtection()));
                }
                a1.interactWith(a2,col,this);
                a2.interactWith(a1,col,this);
            }
            a1.act(this);
        }
        boolean goatAlive = false;
        float minx = Float.MAX_VALUE, maxx = 0, miny = Float.MAX_VALUE, maxy = 0;
        for (int i = 0; i < players.size(); i++) {
            Entity p = players.get(i);
            if(p.isAlive()){
                goatAlive = true;
                if(p.getCenter().x < minx){
                        minx = p.getX();
                }
                if(p.getCenter().x > maxx){
                        maxx = p.getX();
                }
                if(p.getCenter().y < miny){
                        miny = p.getY();
                }
                if(p.getCenter().y > maxy){
                        maxy = p.getY();
                }
            }
        }
        float screenWidth = Gdx.graphics.getWidth();
        float screenHeight = Gdx.graphics.getHeight();
        float neuWidth = maxx-minx+screenWidth;
        float neuHeight = maxy-miny+screenHeight;
        float aspectRatio = (float)screenWidth/(float)screenHeight;
        if(neuWidth> levelSize){
        	neuWidth = levelSize;
        }
        if(neuHeight> levelSize/aspectRatio){
            neuHeight = levelSize/aspectRatio;
        }
        if(neuHeight*aspectRatio > neuWidth){
        	neuWidth = neuHeight*aspectRatio;
        }else{
        	neuHeight = neuWidth/aspectRatio;
        }
        cam.setToOrtho(false, neuWidth, neuHeight);
        Vector2 translate = new Vector2((minx+maxx)/2-neuWidth/2+players.get(0).getBoundingBox().getWidth()/2, (miny+maxy)/2-neuHeight/2+players.get(0).getBoundingBox().getHeight()/2);
        if(translate.x < 0){
        	translate.x = 0;
        }
        if(translate.x + neuWidth > levelSize){
        	translate.x = levelSize-neuWidth;
        }
        if(translate.y > miny-10){
            translate.y = miny-10;
        }
        if(translate.y < 0){
        	translate.y = 0;
        }
        cam.translate(translate);
        
        particles.act();
        if(!trash.isEmpty()){
            entities.removeAll(trash);
            trash.clear();
        }
        
        if(castleEntity.getHealth()<=0){
            screen.showLoseScreen(false);
            for (Entity entity : castleParts) {
                if(entity != castleEntity){
                    entities.remove(entity);
                }
            }
            castleParts.clear();
            castleEntity.setBreaking(true);
        }else if(!goatAlive){
            screen.showLoseScreen(true);
        }
    }
    public void render(){
        cam.update();
        AbstractScreen.getBatch().setProjectionMatrix(cam.combined);
        
        for(Entity ent : entities){
            if(!ent.isForeground()){
                ent.render(this);
            }
        }
        particles.render();
        for(Entity ent : entities){
            if(ent.isForeground()){
                ent.render(this);
            }
        }
        map.renderMap();
        for(Atom atom : atoms){
            atom.render(this);
        }
        
        cam.setToOrtho(false);
        AbstractScreen.getBatch().setProjectionMatrix(cam.combined);
        
        for(HealthBar healthBar : healthBars){
        	healthBar.render();
        }
        castleHealthBar.render();
        
    }
    
    public void resize(){
    	float healthBarWidth = 13*5;
        if(healthBars.size() == 1){
            HealthBar healthBar = healthBars.get(0);
            healthBar.setX(Gdx.graphics.getWidth()/2-healthBarWidth/2);
        }else{
        	float widthSpace = Gdx.graphics.getWidth()-10*2-healthBarWidth;
	        for (int i = 0; i < healthBars.size(); i++) {
	            HealthBar healthBar = healthBars.get(i);
	            healthBar.setX(10+(widthSpace/(healthBars.size()-1))*i);
	        }
        }
        castleHealthBar.setX(Gdx.graphics.getWidth()/2-(16*4)/2);
        castleHealthBar.setY(Gdx.graphics.getHeight()-10-8*4);
    }
    
    public void addEntity(Entity ent){
        inbox.add(ent);
    }
    public void removeEntity(Entity ent){
        trash.add(ent);
    }
    public void spawnEnemy(int type){
        Entity ent = EnemyFactory.createEnemy(type);
        ent.setX(1536+100+(float)(Math.random()*50));
        ent.setY(200+(float)(Math.random()*200));
        addEntity(ent);
    }

    //Getter & Setter
    public void addPlayer(Entity player) {
        this.players.add(player);
        HealthBar healthBar = new HealthBar(10+(13*5)*(players.size()-1),10,"Health Bar/Bar",5);
        healthBar.setParent(player);
        healthBars.add(healthBar);
    }

    public List<Entity> getPlayers() {
        return players;
    }
    
    public List<Entity> getAliveTargets(){
        List<Entity> alivePlayers = new ArrayList<Entity>();
        for (Entity player : players) {
            if(player.isAlive()){
                alivePlayers.add(player);
            }
        }
        if(castleEntity.isAlive()){
            alivePlayers.addAll(castleParts);
        }
        return alivePlayers;
    }

    public ParticleManager getParticles() {
        return particles;
    }
    
    public void damageCastle(float damage){
        castleEntity.damage(damage);
    }
    
    public void addCastlePart(Entity ent){
        castleParts.add(ent);
    }
    
    public boolean eatGrass(Entity ent){
        return map.eatGrass(ent);
    }

    public void setCastleEntity(BreakingCastle castleEntity) {
        this.castleEntity = castleEntity;
        castleEntity.setMaxHealth(14*2);//CastleLife
        castleHealthBar.setParent(castleEntity);
    }
}
