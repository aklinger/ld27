package com.aa.ld27.logic;

import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.stuff.Entity;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

/**
 * The HealthBar.
 * 
 * @author Toni
 */
public class HealthBar {
    //Attributes
    private float x;
    private float y;
    private Entity parent;
    private int pixely;
    
    Array<TextureAtlas.AtlasRegion> findRegions;
    
    //Constructors
    public HealthBar(float x, float y, String picture, int pixely) {
        this.x = x;
        this.y = y;
        
        this.pixely = pixely;
        
        findRegions = AbstractScreen.getAtlas().findRegions(picture);
    }
    
    //Methods
    public void render(){
        if(parent != null){
            int frames = findRegions.size - 1;
            int result = (int)Math.round((parent.getHealth()/parent.getMaxHealth())*frames);
            if(result < 0){
                result = 0;
            }
            AbstractScreen.getBatch().begin();
            try{
                Drawable drawable = new TextureRegionDrawable(findRegions.get(result));
                drawable.draw(AbstractScreen.getBatch(), x, y, drawable.getMinWidth()*pixely, drawable.getMinHeight()*pixely);
            }finally{
                AbstractScreen.getBatch().end();
            }
        }
    }

    //Getter & Setter
    public void setParent(Entity parent) {
        this.parent = parent;
    }

    public void setX(float x) {
        this.x = x;
    }

    public void setY(float y) {
        this.y = y;
    }
    
}
