package com.aa.ld27.map;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * A advanced version of the ViewPort, which also supports shaking the screen.
 * 
 * @author Toni
 */
public class GameView extends ViewPort{
    //Attribute
    private Vector2 shake;
    private float shakePower;
    private float shakeReduction;
    //
    private boolean shakeScreen;
    //Konstruktor
    public GameView(Vector2 dimension, Vector2 scroll, Vector2 zoom) {
        super(dimension, scroll, zoom);
    }
    public GameView(Vector2 dimension) {
        super(dimension);
    }
    {
        shakeScreen = true;
        //
        shake = Vector2.Zero;
        shakePower = 0;
        shakeReduction = 0;
    }
    //Methoden
    public void act(){
        shakePower -= shakeReduction;
        if(shakePower <= 0){
            shakePower = 0;
            shake = Vector2.Zero;
        }else{
            shake = new Vector2((float)Math.random(),(float)Math.random()).mul(shakePower).sub(shakePower/2,shakePower/2);
        }
    }
    public void shake(float intensity, float time){
        shakePower = intensity;
        shakeReduction = intensity/time;
    }
    @Override
    public void setRectangle(Rectangle rect) {
        Vector2 ratio = getDisplayDimension().div(new Vector2(rect.getWidth(),rect.getHeight()));
        if(ratio.x<ratio.y){
            setZoom(ratio.x);
        }else{
            setZoom(ratio.y);
        }
        centerOnPoint(new Vector2(rect.x+rect.width/2,rect.y+rect.height/2));
    }
    //Overrides
    @Override
    public Vector2 getRealScroll() {
        if(shakeScreen){
            return super.getRealScroll().add(shake);
        }else{
            return super.getRealScroll();
        }
    }
    //Getter & Setter
    public boolean isShakeScreen() {
        return shakeScreen;
    }
    public void setShakeScreen(boolean shakeScreen) {
        this.shakeScreen = shakeScreen;
    }
}
