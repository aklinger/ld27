package com.aa.ld27.map;

import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.stuff.Entity;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

/**
 * A really goaty map.
 * <p/>
 * @author Toni
 */
public class GoatMap extends VoxelMap {
    //Attributes
    private float tileSize;

    //Constructors
    public GoatMap(int width, int height, float tileSize) {
        super(width, height);
        this.tileSize = tileSize;
    }

    public static GoatMap createPlainMap(int width, int height, int groundHeight, float tileSize) {
        GoatMap map = new GoatMap(width, height, tileSize);
        for (int i = 0; i < map.getXCount(); i++) {
            for (int j = 0; j < map.getYCount(); j++) {
                if (j < groundHeight - 1) {
                    map.setTile(i, j, Tile.createTile(Tile.BEDROCK));
                } else if (j < groundHeight) {
                    map.setTile(i, j, Tile.createTile(Tile.GROUND));
                } else if (j == groundHeight) {
                    map.setTile(i, j, Tile.createTile(Tile.GRASS));
                } else {
                    map.setTile(i, j, Tile.createTile(Tile.AIR));
                }
            }
        }
        return map;
    }

    //Methods
    public void renderMap() {
        AbstractScreen.getBatch().begin();
        for (int i = 0; i < getXCount(); i++) {
            for (int j = 0; j < getYCount(); j++) {
                Tile tile = getTile(i, j);
                Drawable drawable = tile.getDrawable();
                if (drawable != null) {
                    drawable.draw(AbstractScreen.getBatch(), i * tileSize, j * tileSize, tileSize, tileSize);
                }
            }
        }
        AbstractScreen.getBatch().end();
    }
    public boolean eatGrass(Entity ent){
        Tile tile = this.getTile(ent.getCenter(), tileSize);
        if(tile != null && !tile.isSolid())
        {
            return tile.eatGrass();
        }
        return false;
    }
}
