package com.aa.ld27.map;

import com.aa.ld27.gui.AbstractScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.awt.Color;
import java.util.HashMap;
import java.util.Map;

/**
 * A very basic tile.
 * <p/>
 * @author Toni
 */
public class Tile {
    //Static
    public static final int AIR = 0;
    public static final int GROUND = 1;
    public static final int SPIKES = 2;
    public static final int GAS = 3;
    public static final int GRASS = 4;
    public static final int BEDROCK = 5;
    
    private static TextureAtlas atlas;
    private static Map<String, Drawable> drawableCache;
    //Attribute
    private boolean solid;
    private boolean lethal;
    private Color color;
    private String image;
    private int zustand;
    private boolean grasTile;

    //Konstruktor
    static {
        atlas = AbstractScreen.getAtlas();//new TextureAtlas(Gdx.files.internal("image-atlases/pages-info.atlas"));
        drawableCache = new HashMap<String, Drawable>();
    }

    private Tile(boolean solid, boolean lethal) {
        this.solid = solid;
        this.lethal = lethal;
        color = new Color((int) (Math.random() * 255), (int) (Math.random() * 255), (int) (Math.random() * 255));
    }

    public static Tile createTile(int type) {
        Tile t = null;
        switch (type) {
            case AIR:
                t = new Tile(false, false);
                t.image = null;
                break;
            case GROUND:
                t = new Tile(true, false);
                t.image = "base_grass";
                int range = 50;
                t.color = new Color((int) (Math.random() * range) + 100 - range / 2, (int) (Math.random() * range) + 150 - range / 2, (int) (Math.random() * range) + 200 - range / 2);
                break;
            case BEDROCK:
                t = new Tile(true, false);
                t.image = "base_ground";
                range = 30;
                t.color = new Color((int) (Math.random() * range) + 100 - range / 2, (int) (Math.random() * range) + 150 - range / 2, (int) (Math.random() * range) + 200 - range / 2);
                break;
            case SPIKES:
                t = new Tile(true, true);
                t.image = "Grass_small";
                t.color = new Color((int) (Math.random() * 50) + 200, (int) (Math.random() * 80) + 20, (int) (Math.random() * 20) + 10);
                break;
            case GAS:
                t = new Tile(false, true);
                t.image = "Grass_small";
                break;
            case GRASS:
                t = new Tile(false, false);
                t.zustand = (int)(Math.random()*4)+1;
                t.grasTile = true;
                t.updatePicture();
                break;
            default:
                throw new RuntimeException("Unknown Tile-Type: " + type);
        }
        return t;
    }
    
    //Methoden
    public Drawable getDrawable() {
        String key = image;
        if(key == null){
            return null;
        }
        Drawable draw = drawableCache.get(key);
        if (draw == null) {
            TextureRegion region = atlas.findRegion(key);
            draw = new TextureRegionDrawable(region);
            drawableCache.put(key, draw);
        }
        return draw;
    }
    public boolean eatGrass(){
        if(grasTile && zustand > 0){
            zustand--;
            updatePicture();
            return true;
        }
        return false;
    }
    private void updatePicture(){
        if(grasTile){
            if(zustand >= 4){
                image = "Grass_big";
            }else if(zustand == 3){
                image = "Grass_medium";
            }else if(zustand == 2){
                image = "Grass_small";
            }else if(zustand == 1){
                image = "Grass_tiny";
            }else if(zustand <= 0){
                image = "Grass_nano";
            }else {
                image = null;
            }
        }
    }
    
    //Getter & Setter
    public boolean isSolid() {
        return solid;
    }

    public void setSolid(boolean solid) {
        this.solid = solid;
    }

    public boolean isLethal() {
        return lethal;
    }

    public void setLethal(boolean lethal) {
        this.lethal = lethal;
    }

    public Color getColor() {
        return color;
    }
}
