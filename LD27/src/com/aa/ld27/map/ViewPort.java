package com.aa.ld27.map;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import java.io.Serializable;

/**
 * A class for managing the translation and scaling of the camera.
 * <p/>
 * @author Toni
 */
public class ViewPort implements Serializable {
    //Attribute
    private static final long serialVersionUID = 0x1337;
    //
    private Vector2 dimension;//The width and height of the Output-Device
    private Vector2 scroll;//The translation of the Viewport. Aka: distance from the Left Upper Corner to the coordinate-root in real scale
    private Vector2 zoom;//The value by which every point is multiplied.
    //Konstruktor
    /**
     * Creates a new Viewport with the specific dimension.
     * <p/>
     * @param dimension The Dimension of the Viewport.
     */
    public ViewPort(Vector2 dimension) {
        this(dimension, new Vector2(0, 0), new Vector2(1, 1));
    }
    /**
     * Creates a new Viewport with the specific dimension, scrolling and zoom.
     * <p/>
     * @param dimension The Dimension of the Viewport.
     * @param scroll The translation of the Viewport.
     * @param zoom The scaling of the Viewport.
     */
    public ViewPort(Vector2 dimension, Vector2 scroll, Vector2 zoom) {
        this.dimension = dimension;
        this.scroll = scroll;
        this.zoom = zoom;
    }
    //Methoden
    /**
     * Applies the ViewPort to the current Projection-Matrix.
     */
    public void applyView() {
        //TODO save scroll already flipped, so it doesn't have to be flipped every frame.
        //MatrixMod.applyZoom(zoom);
        //MatrixMod.applyTranslation(getRealScroll().mul(-1));
        Gdx.gl10.glScalef(zoom.x, zoom.y, 1);
        Gdx.gl10.glTranslatef(-getRealScroll().x,-getRealScroll().y, 0);
    }
    /**
     * Converts a Point on the Display to a Point in the game area. Useful for
     * converting Mouse-Positions.
     * <p/>
     * @param p The Point on the real display
     * @return A new Vektor representing the Point in the virtual game area
     */
    public Vector2 calcVirtualPoint(Vector2 p) {
        return p.add(getVirtualScroll()).div(zoom);
    }
    /**
     * Converts a Point on the game area to a Point on the Display
     * <p/>
     * @param p The Point in the virtual game area
     * @return A new Vektor representing the Point on the real display
     */
    public Vector2 calcRealPoint(Vector2 p) {
        Vector2 point = p.cpy();
        point.x = point.x * zoom.x - getVirtualScroll().x;
        point.y = point.y * zoom.y - getVirtualScroll().y;
        return point;
        //return p.mul(zoom).sub(getVirtualScroll());
    }
    public void setRectangle(Rectangle rect) {
        Vector2 display = getDisplayDimension();
        setZoom(display.div(rect.getWidth(),rect.getHeight()));
        setRealScroll(new Vector2(rect.getX(), rect.getY()));
    }
    public Rectangle getRectangle() {
        Vector2 vScroll = getVirtualScroll();
        Vector2 vDim = getVirtualDimension();
        Rectangle rect = new Rectangle((int) vScroll.x, (int) vScroll.y, (int) vDim.x, (int) vDim.y);
        return rect;
    }
    public void centerOnPoint(Vector2 point) {
        setRealScroll(point.sub(getVirtualDimension().div(2)));
    }
    /*public void changeZoom(double zoom, Vector2 point) {
        //TODO write this method correctly
        /*
         * context.translate( originx, originy ); context.scale(zoom,zoom);
         * context.translate( -( mousex / scale + originx - mousex / ( scale *
         * zoom ) ), -( mousey / scale + originy - mousey / ( scale * zoom ) )
         * );
         *
         * originx = ( mousex / scale + originx - mousex / ( scale * zoom ) );
         * originy = ( mousey / scale + originy - mousey / ( scale * zoom ) );
         * scale *= zoom;
         *
        setRealScroll(point.div(zoom).add(getRealScroll()).sub(point).div(getZoom().mul(zoom)));
        setZoom(zoom);
    }*/
    //Special Getter & Setter
    public void setZoom(float zoom) {
        this.zoom = new Vector2(zoom, zoom);
    }
    public Vector2 getVirtualDimension() {
        return getDisplayDimension().div(getZoom());
    }
    public Vector2 getVirtualScroll() {
        return getRealScroll().cpy().mul(getZoom().x,getZoom().y);
    }
    public void setVirtualScroll(Vector2 scroll) {
        setRealScroll(scroll.div(getZoom()));
    }
    //Getter & Setter
    public Vector2 getDisplayDimension() {
        return dimension.cpy();
    }
    public void setDisplayDimension(Vector2 dimension) {
        this.dimension = dimension;
    }
    public Vector2 getRealScroll() {
        return scroll.cpy();
    }
    public void setRealScroll(Vector2 scroll) {
        this.scroll = scroll;
    }
    public Vector2 getZoom() {
        return zoom;
    }
    public void setZoom(Vector2 zoom) {
        this.zoom = zoom;
    }
}
