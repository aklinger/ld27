package com.aa.ld27.map;

import com.badlogic.gdx.math.Vector2;

/**
 * A basic 3D map data.
 * <p/>
 * @author Toni
 */
public class VoxelMap {
    //Attribute
    private Tile[][] map;

    //Konstruktor
    public VoxelMap(int width, int height) {
        map = new Tile[width][height];
    }

    //Methoden
    public Tile getTile(Vector2 position, double tileSize) {
        return getTile((int) (position.x / tileSize), (int) (position.y / tileSize));
    }

    public Tile getTile(int x, int y) {
        if (isWithinMapBounds(x, y)) {
            return map[x][y];
        }
        return null;
    }

    public boolean isWithinMapBounds(int x, int y) {
        if (x >= 0 && x < getXCount()) {
            if (y >= 0 && y < getYCount()) {
                return true;
            }
        }
        return false;
    }
    public boolean isWithinMapBounds(Vector2 indexPosition){
        return isWithinMapBounds((int)(indexPosition.x), (int)(indexPosition.y));
    }

    public void setTile(int x, int y, Tile value) {
        map[x][y] = value;
    }

    //Getter & Setter
    public int getXCount() {
        return map.length;
    }

    public int getYCount() {
        return map[0].length;
    }
}
