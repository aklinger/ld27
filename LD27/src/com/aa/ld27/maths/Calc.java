package com.aa.ld27.maths;

/**
 * My own version of the java.lang.Math class. Everything that should be there
 * gets added here.
 * <p/>
 * If a Method is prefixed "fast", it often trades accuracy or error-security
 * for speed.
 * <p/>
 * @author Toni
 */
public class Calc {
    //Attribute
    /**
     * The most random number I know...
     */
    public static final double RANDOM = 37;
    /**
     * A complete Circle in Radians.
     */
    public static final double TWO_PI = Math.PI * 2;
    //Konstruktor
    /**
     * This is a utility-class, therefore no instancing.
     */
    private Calc() {
    }
    //Statische Methoden
    /**
     * Faster implementation than Math.floor()
     * <p/>
     * Ignores cases like when value is infinity.
     * <p/>
     * @param number The number to round down.
     * @return The next Integer that is smaller or equal to number.
     */
    public static int fastFloor(double number) {
        return (number < 0) ? (number == (int) number) ? (int) number : (int) (number - 1) : (int) number;
    }
    /**
     * Faster implementation than Math.ceil().
     * <p/>
     * Ignores cases like when value is infinity.
     * <p/>
     * @param number The number to round up.
     * @return The next Integer that is greater or equal to number.
     */
    public static int fastCeil(double number) {
        return (number == (int) (number)) ? (int) number : (number < 0) ? (int) number : (int) (number + 1);
    }
    /**
     * Faster implementation than Math.max() for double values.
     * <p/>
     * Ignores cases like when values are infinity.
     * <p/>
     * @param a The first number
     * @param b The second number
     * @return The larger Number of both arguments.
     */
    public static double fastMax(double a, double b) {
        return (a >= b) ? a : b;
    }
    /**
     * Faster implementation than Math.min() for double values.
     * <p/>
     * Ignores cases like when values are infinity.
     * <p/>
     * @param a The first number
     * @param b The second number
     * @return The smaller Number of both arguments.
     */
    public static double fastMin(double a, double b) {
        return (a <= b) ? a : b;
    }
    /**
     * Ensures that the returned value is within a certain range.
     * <p/>
     * Uses fastMin() and fastMax().
     * <p/>
     * @param value The value to fit into the range.
     * @param range1 The first constraint of the range.
     * @param range2 The second constraint of the range.
     * @return A value that is within the range.
     */
    public static double fastRange(double value, double range1, double range2) {
        if (range1 < range2) {
            return fastMin(range2, fastMax(range1, value));
        } else {
            return fastMin(range1, fastMax(range2, value));
        }
    }
    /**
     * Squares the input value.
     * <p/>
     * @param a The value to square
     * @return The value squared (a*a)
     */
    public static double square(double a) {
        return a * a;
    }
}
