package com.aa.ld27.maths;

import com.aa.ld27.stuff.Entity;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

/**
 * An interface to mark Classes as having Hitboxes.
 * <p/>
 * @author Toni
 */
public interface HitBox {
    //Konstanten
    int AABB = 1;
    int CIRCLE = 2;
    int LINE = 3;
    //Abstrakte Methoden
    Entity getParent();
    Rectangle getBoundingBox();
    Vector2 getCenter();
    int getType();
    //Used for Lines
    Vector2 getDelta();
    Vector2 getPoint1();
}
