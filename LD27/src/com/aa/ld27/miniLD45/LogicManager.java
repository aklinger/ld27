package com.aa.ld27.miniLD45;

import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.particle.ParticleList;
import com.aa.ld27.particle.ParticleManager;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;

/**
 * Manages all entities that are in the game at any given moment.
 * 
 * @author Toni
 */
public class LogicManager {
    //Attribute
    private ParticleManager particles;
    
    private LogicTile[][] map;
    private OrthographicCamera cam;
    private LogicTile currentTile;
    
    private float tileSize = 64;
    
    private float originalWidth;
    private float originalHeight;
    
    private boolean pressed = false;
    
    //Konstruktor
    public LogicManager() {
        particles = new ParticleList();
        
        int levelwidth = 12;
        int levelheight = 9;
        this.map = new LogicTile[levelwidth][levelheight];
        originalWidth = Gdx.graphics.getWidth();
        originalHeight = Gdx.graphics.getHeight();
        cam = new OrthographicCamera(originalWidth, originalHeight);
        cam.translate(Gdx.graphics.getWidth()/2, Gdx.graphics.getHeight()/2);
        cam.update();
        
        /*for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                map[i][j] = LogicTile.createLogicTile((int)(Math.random()*5)+1);
            }
        }*/
        
        currentTile = LogicTile.createLogicTile((int)(Math.random()*5)+1);
    }
    
    //Methoden
    public void act(){
        int mouseX = Gdx.input.getX();
        int mouseY = Gdx.input.getY();
        mouseY = (mouseY-Gdx.graphics.getHeight())*-1;
        if(Gdx.input.isButtonPressed(Input.Buttons.LEFT)){
            if(!pressed){
                pressed = true;
                int xPos = (int)(mouseX/(tileSize*(Gdx.graphics.getWidth()/originalWidth)));
                int yPos = (int)(mouseY/(tileSize*(Gdx.graphics.getHeight()/originalHeight)));
                if(xPos >= 0 && yPos >= 0 && xPos < map.length && yPos < map[0].length){
                    LogicTile t = map[xPos][yPos];
                    if(t == null){
                        setTile(xPos,yPos,currentTile);
                        currentTile = LogicTile.createLogicTile((int)(Math.random()*5)+1);
                    }
                }
            }
        }else{
            pressed = false;
        }
        currentTile.setX(mouseX/(Gdx.graphics.getWidth()/originalWidth));
        currentTile.setY(mouseY/(Gdx.graphics.getHeight()/originalHeight));
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                LogicTile t = map[i][j];
                if(t != null){
                    t.setxPos(i);
                    t.setyPos(j);
                    t.act(this);
                }
            }
        }
        checkForMatches();
        particles.act();
    }
    private void checkForMatches(){
        int neededForMatch = 5;
        for (int i = 0; i < LogicTile.directions.length; i++) {
            Vector2 v = LogicTile.directions[i];
            for (int j = 0; j < map.length; j++) {
                outer:
                for (int k = 0; k < map[0].length; k++) {
                    LogicTile t = map[j][k];
                    if(t != null){
                        boolean isOn = t.isOn();
                        if(isOn){
                            int[][] tiles = new int[neededForMatch][2];
                            int count = 0;
                            for (int l = 0; l < neededForMatch; l++) {
                                int nx = j+(int)(v.x*l);
                                int ny = k+(int)(v.y*l);
                                if(nx >= 0 && ny >= 0 && nx < map.length && ny < map[0].length){
                                    LogicTile other = map[nx][ny];
                                    tiles[l][0] = nx;
                                    tiles[l][1] = ny;
                                    count++;
                                    if(other == null || other.isOn() != isOn){
                                        continue outer;
                                    }
                                }else{
                                    continue outer;
                                }
                            }
                            if(count == neededForMatch){
                                for (int weg = 0; weg < tiles.length; weg++) {
                                    map[tiles[weg][0]][tiles[weg][1]] = null;
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    public void render(){
        cam.update();
        AbstractScreen.getBatch().setProjectionMatrix(cam.combined);
        
        for (int i = 0; i < map.length; i++) {
            for (int j = 0; j < map[i].length; j++) {
                LogicTile t = map[i][j];
                if(t != null){
                    t.setX(i*tileSize);
                    t.setY(j*tileSize);
                    t.setDrawableSize(tileSize, tileSize);
                    t.render(this);
                }
            }
        }
        
        currentTile.setDrawableSize(tileSize/2, tileSize/2);
        currentTile.render(this);
        particles.render();
    }
    
    //Getter & Setter
    public ParticleManager getParticles() {
        return particles;
    }

    public LogicTile[][] getMap() {
        return map;
    }
    
    public LogicTile getTile(int x, int y){
        if(x >= 0 && y >= 0 && x < map.length && y < map[0].length){
            LogicTile t = map[x][y];
            if(t != null){
                return t;
            }else{
                return LogicTile.createLogicTile(LogicTile.OFF);
            }
        }else{
            return LogicTile.createLogicTile(LogicTile.OFF);
        }
    }
    
    public void setTile(int x, int y, LogicTile tile){
        if(x >= 0 && y >= 0 && x < map.length && y < map[0].length){
            map[x][y] = tile;
        }
    }
}
