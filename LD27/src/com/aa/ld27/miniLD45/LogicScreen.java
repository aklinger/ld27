/*
 * To change this default template go to 
 * Tools|Templates|Licenses|Default License
 */

package com.aa.ld27.miniLD45;

import com.aa.ld27.GameLoop;
import com.aa.ld27.dal.MusicManager;
import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.gui.MenuScreen;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

/**
 *
 * @author Toni
 */
public class LogicScreen extends AbstractScreen{
    //Attributes
    private LogicManager manager;
    //Constructors
    
    //Methoden
    @Override
    public void render(float delta) {
        super.render(delta);
        
        manager.act();
        manager.render();
        
        
        if(Gdx.input.isKeyPressed(Input.Keys.ESCAPE)){
            GameLoop.setCurrentScreen(new MenuScreen());
        }
        if(Gdx.input.isKeyPressed(Input.Keys.M)){
        	MusicManager.getInstance().stop();
        }
    }
    
    @Override
    public void show() {
        super.show();
        
        manager = new LogicManager();
    }
    
    //Getter & Setter
    
    
}
