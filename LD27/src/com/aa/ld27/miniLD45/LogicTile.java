package com.aa.ld27.miniLD45;

import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.logic.EntityManager;
import static com.aa.ld27.stuff.Entity.removeAllBadIndizes;
import com.aa.ld27.stuff.Fanta;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 *
 * @author Toni
 */
public class LogicTile {
    //General Movement Attributes
    private float x;
    private float y;
    private float sx;
    private float sy;
    private float gx;
    private float gy;
    private float fx;
    private float fy;
    //Render Attributes
    private Fanta fanta;
    private Color color;
    private float imgW;
    private float imgH;
    private float rotation;
    private boolean flipX;
    private boolean flipY;
    //asdf
    private boolean on;
    private int type;
    private Vector2[] inputs;
    private int xPos;
    private int yPos;
    //Direction
    private static final Vector2 DIR_UP = new Vector2(0, 1);
    private static final Vector2 DIR_DOWN = new Vector2(0, -1);
    private static final Vector2 DIR_LEFT = new Vector2(-1, 0);
    private static final Vector2 DIR_RIGHT = new Vector2(1, 0);
    public static final Vector2[] directions = {DIR_UP, DIR_DOWN, DIR_LEFT, DIR_RIGHT};
    //type
    public static final int OFF = 0;
    public static final int ON = 1;
    public static final int OR = 2;
    public static final int AND = 3;
    public static final int XOR = 4;
    public static final int NOT = 5;
    private static final Fanta IMAGE_ON = loadSingleImage("logic/active_block");
    private static final Fanta IMAGE_OFF = loadSingleImage("logic/deactive_block");
    private static final Fanta IMAGE_CONNECTOR = loadSingleImage("logic/connector");

    //Konstruktor
    public LogicTile() {
        fx = 1;
        fy = 1;
        color = Color.WHITE;
    }

    public static LogicTile createLogicTile(int type) {
        LogicTile tile = new LogicTile();
        tile.type = type;
        switch (type) {
            case OFF:
                tile.on = false;
                tile.inputs = new Vector2[0];
                break;
            case ON:
                tile.on = true;
                tile.inputs = new Vector2[0];
                break;
            case OR:
                //Load OR label
                tile.fanta = loadSingleImage("logic/or_label");
                getRandomInputs(tile, 2);
                break;
            case AND:
                //Load AND label
                tile.fanta = loadSingleImage("logic/and_label");
                getRandomInputs(tile, 2);
                break;
            case XOR:
                //Load XOR label
                tile.fanta = loadSingleImage("logic/xor_label");
                getRandomInputs(tile, 2);
                break;
            case NOT:
                //Load NOT label
                tile.fanta = loadSingleImage("logic/not_label");
                getRandomInputs(tile, 1);
                break;
        }
        return tile;
    }

    private static void getRandomInputs(LogicTile tile, int count) {
        int[] rands = new int[count];
        tile.inputs = new Vector2[count];
        for (int i = 0; i < rands.length; i++) {
            rands[i] = -1;
        }
        for (int i = 0; i < count; i++) {
            int nextR = (int) (Math.random() * (directions.length - i));
            for (int j = 0; j < rands.length; j++) {
                if (rands[j] != -1) {
                    if (nextR >= rands[j]) {
                        nextR++;
                    }
                }
            }
            tile.inputs[i] = directions[nextR];
            rands[i] = nextR;
        }
    }

    public static Fanta loadSingleImage(String atlasIndex) {
        Fanta fanta = new Fanta(atlasIndex);
        TextureAtlas.AtlasRegion findRegion = AbstractScreen.getAtlas().findRegion(atlasIndex);
        fanta.putTextureRegion(atlasIndex, findRegion);
        return fanta;
    }

    public static Fanta loadSingleAnimation(String atlasIndex, int frameDuration) {
        Fanta fanta = new Fanta(atlasIndex);
        Array<TextureAtlas.AtlasRegion> findRegions = AbstractScreen.getAtlas().findRegions(atlasIndex);
        removeAllBadIndizes(findRegions);
        Animation anim = new Animation(frameDuration, findRegions, Animation.LOOP);
        fanta.putAnimation(atlasIndex, anim);
        return fanta;
    }

    public static void removeAllBadIndizes(Array<TextureAtlas.AtlasRegion> regions) {
        for (int i = regions.size - 1; i >= 0; i--) {
            TextureAtlas.AtlasRegion ar = regions.get(i);
            if (ar.index == -1) {
                regions.removeIndex(i);
            }
        }
    }

    //Methoden
    public void computeMovement() {
        sx += gx;
        sy += gy;
        sx *= fx;
        sy *= fy;
        x += sx;
        y += sy;
    }

    public void act(LogicManager man) {
        if(fanta != null){
            fanta.act(1);
        }
        on = checkOnCondition(man);
    }

    public boolean checkOnCondition(LogicManager man) {
        switch (type) {
            case OFF:
                return false;
            case ON:
                return true;
            case OR:
                for (int i = 0; i < inputs.length; i++) {
                    if(man.getTile((int)(xPos+inputs[i].x),(int)(yPos+inputs[i].y)).isOn()){
                        return true;
                    }
                }
                return false;
            case AND:
                for (int i = 0; i < inputs.length; i++) {
                    if(!man.getTile((int)(xPos+inputs[i].x),(int)(yPos+inputs[i].y)).isOn()){
                        return false;
                    }
                }
                return true;
            case XOR:
                int count = 0;
                for (int i = 0; i < inputs.length; i++) {
                    if(man.getTile((int)(xPos+inputs[i].x),(int)(yPos+inputs[i].y)).isOn()){
                        count++;
                    }
                }
                return count % 2 == 1;
            case NOT:
                return !man.getTile((int)(xPos+inputs[0].x),(int)(yPos+inputs[0].y)).isOn();
        }
        System.out.println("RANDOM!");
        return Math.random() > 0.1;
        //Alternatively, the conditions on the blocks could just be ">1" "=2" "<2" "!=1" "%2"
    }

    public void render(LogicManager man) {
        AbstractScreen.getBatch().begin();
        try {
            drawTheThing();
        } finally {
            AbstractScreen.getBatch().end();
        }
    }

    protected void drawTheThing() {
        AbstractScreen.getBatch().setColor(color);
        float originX = imgW / 2;
        float originY = imgH / 2;
        TextureRegion base;
        if (on) {
            base = IMAGE_ON.getCurrentFrame();
        } else {
            base = IMAGE_OFF.getCurrentFrame();
        }
        base.getTexture();
        AbstractScreen.getBatch().draw(base.getTexture(), getX(), getY(), originX, originY, imgW, imgH, 1, 1, rotation, base.getRegionX(), base.getRegionY(), base.getRegionWidth(), base.getRegionHeight(), flipX, flipY);
        
        if(fanta != null){
            TextureRegion currentFrame = fanta.getCurrentFrame();
            AbstractScreen.getBatch().draw(currentFrame.getTexture(), getX(), getY(), originX, originY, imgW, imgH, 1, 1, rotation, currentFrame.getRegionX(), currentFrame.getRegionY(), currentFrame.getRegionWidth(), currentFrame.getRegionHeight(), flipX, flipY);
        }
        
        for (int i = 0; i < inputs.length; i++) {
            float rot = 0;
            if(inputs[i].equals(DIR_DOWN)){
                rot = 180;
            }else if(inputs[i].equals(DIR_LEFT)){
                rot = 90;
            }else if(inputs[i].equals(DIR_RIGHT)){
                rot = 270;
            }
            TextureRegion a = IMAGE_CONNECTOR.getCurrentFrame();
            AbstractScreen.getBatch().draw(a.getTexture(), getX(), getY(), originX, originY, imgW, imgH, 1, 1, rot, a.getRegionX(), a.getRegionY(), a.getRegionWidth(), a.getRegionHeight(), flipX, flipY);
        }
        AbstractScreen.getBatch().setColor(Color.WHITE);
    }

    public void die(EntityManager man) {
        //This happens after it dies.//Maybe spawn particles
    }

    //Adders
    public void rotatoToDirectionMoving(float offset) {
        float rot = (float) Math.toDegrees(Math.atan2(-getSx(), getSy()));
        rotation = rot + offset;
    }

    public void addPosition(float deltax, float deltay) {
        x += deltax;
        y += deltay;
    }

    public void addPosition(Vector2 delta) {
        x += delta.x;
        y += delta.y;
    }

    public void addSpeed(Vector2 delta) {
        addSpeed(delta.x, delta.y);
    }

    public void addSpeed(float dx, float dy) {
        this.sx += dx;
        this.sy += dy;
    }

    public void rotate(float deltaRotation) {
        rotation += deltaRotation;
    }
    //Getter & Setter

    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getSx() {
        return sx;
    }

    public void setSx(float sx) {
        this.sx = sx;
    }

    public float getSy() {
        return sy;
    }

    public void setSy(float sy) {
        this.sy = sy;
    }

    public float getGx() {
        return gx;
    }

    public void setGx(float gx) {
        this.gx = gx;
    }

    public float getGy() {
        return gy;
    }

    public void setGy(float gy) {
        this.gy = gy;
    }

    public float getFx() {
        return fx;
    }

    public void setFx(float fx) {
        this.fx = fx;
    }

    public float getFy() {
        return fy;
    }

    public void setFy(float fy) {
        this.fy = fy;
    }

    public float getRotation() {
        return rotation;
    }

    public void setRotation(float rotation) {
        this.rotation = rotation;
    }

    public boolean isFlipX() {
        return flipX;
    }

    public void setFlipX(boolean flipX) {
        this.flipX = flipX;
    }

    public boolean isFlipY() {
        return flipY;
    }

    public void setFlipY(boolean flipY) {
        this.flipY = flipY;
    }

    public void setDrawableSize(float width, float height) {
        this.imgW = width;
        this.imgH = height;
    }

    public boolean isForeground() {
        return false;
    }

    public Fanta getFanta() {
        return fanta;
    }

    public void setFanta(Fanta fanta) {
        this.fanta = fanta;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public float getImgW() {
        return imgW;
    }

    public float getImgH() {
        return imgH;
    }

    public void setxPos(int xPos) {
        this.xPos = xPos;
    }

    public void setyPos(int yPos) {
        this.yPos = yPos;
    }

    public boolean isOn() {
        return on;
    }
    
}
