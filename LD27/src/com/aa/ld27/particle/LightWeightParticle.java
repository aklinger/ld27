package com.aa.ld27.particle;

import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.Calc;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import java.util.ArrayDeque;
import java.util.Collection;

/**
 * A lightWeight implementation of a Particle.
 * <p/>
 * It only has what it needs.
 * <p/>
 * @author Toni
 */
public class LightWeightParticle implements Particle {
    //Movement-Attribute
    private float x;
    private float y;
    private float sx;
    private float sy;
    private float fx;
    private float fy;
    private float gx;
    private float gy;
    private float rotation;
    private float rotationSpeed;
    private float rotationFriction;
    private float rotationGravitation;
    //
    private int time;
    private float alpha = 1f;
    private float alphaReduction;
    //
    private Sprite sprite;
    //Konstruktor
    public LightWeightParticle(float x, float y, int time, String name, boolean fadeOut) {
        this.x = x;
        this.y = y;
        this.fx = 1;
        this.fy = 1;
        this.time = time;
        if(fadeOut){
            alphaReduction = alpha/time;
        }
        TextureRegion region = AbstractScreen.getAtlas().findRegion(name);
        this.sprite = new Sprite(region);
    }
    public static Collection<LightWeightParticle> particleExplosion(EntityManager man, float amount, float x, float y, float spawnDisplacement, double direction, double angleRange, float minSpeed, float maxSpeed, float gravity, String picture, int time, boolean fadeOut, float rotationSpeed, float size){
        Collection parts = new ArrayDeque();
        for (int i = 0; i < amount; i++) {
            double spawnDirection = Math.random()*Calc.TWO_PI;
            float posx = x+(float)(Math.sin(spawnDirection)*(Math.random()*spawnDisplacement));
            float posy = y+(float)(Math.cos(spawnDirection)*(Math.random()*spawnDisplacement));
            LightWeightParticle p = new LightWeightParticle(posx,posy,(int)(Math.random()*time*0.75+time*0.25),picture,fadeOut);
            
            float neuDirection = (float)(direction+Math.random()*angleRange-angleRange/2);
            float spx = (float)(Math.sin(neuDirection)*(Math.random()*(maxSpeed-minSpeed)+minSpeed));
            float spy = (float)(Math.cos(neuDirection)*(Math.random()*(maxSpeed-minSpeed)+minSpeed));
            p.setSx(spx);
            p.setSy(spy);
            p.setGy(gravity);
            p.setRotationSpeed((float)(Math.random()*rotationSpeed*2)-rotationSpeed);
            p.setSize(size, size);
            parts.add(p);
        }
        man.getParticles().addAllParticles(parts);
        return parts;
    }
    public static LightWeightParticle createStaticParticle(EntityManager man, float x, float y, float spawnDisplacement, String picture, int time, boolean fadeOut, float size){
        double spawnDirection = Math.random()*Calc.TWO_PI;
        float posx = x+(float)(Math.sin(spawnDirection)*(Math.random()*spawnDisplacement));
        float posy = y+(float)(Math.cos(spawnDirection)*(Math.random()*spawnDisplacement));
        LightWeightParticle p = new LightWeightParticle(posx,posy,(int)(Math.random()*time*0.75+time*0.25),picture,fadeOut);
        p.setSize(size,size);
        
        man.getParticles().addParticle(p);
        
        return p;
    }
    //Methoden
    @Override
    public void act() {
        calcMovement();
        rotationSpeed = (rotationSpeed * rotationFriction) + rotationGravitation;
        rotation += rotationSpeed;
        time--;
        alpha -= alphaReduction;
    }
    private void calcMovement() {
        sx = (sx * fx) + gx;
        sy = (sy * fy) + gy;
        x += sx;
        y += sy;
    }
    @Override
    public void render() {
    	AbstractScreen.getBatch().begin();
        try{
            Gdx.gl10.glTranslatef(x+sprite.getWidth()/2, y+sprite.getHeight()/2, 0);
            Gdx.gl11.glRotatef(rotation, 0, 0, 1);
            Gdx.gl10.glTranslatef(-x-sprite.getWidth()/2, -y-sprite.getHeight()/2, 0);
            sprite.setPosition(x, y);
            Color c = sprite.getColor();
            sprite.setColor(c.r, c.g, c.b, alpha);
            sprite.draw(AbstractScreen.getBatch());
        }finally{
            AbstractScreen.getBatch().end();
        }
    }
    @Override
    public boolean isActive() {
        return time >= 0 && alpha >= 0;
    }
    //Getter & Setter
    public void setFx(float fx) {
        this.fx = fx;
    }
    public void setFy(float fy) {
        this.fy = fy;
    }
    public void setGx(float gx) {
        this.gx = gx;
    }
    public void setGy(float gy) {
        this.gy = gy;
    }
    public void setSx(float sx) {
        this.sx = sx;
    }
    public void setSy(float sy) {
        this.sy = sy;
    }
    public void setTime(int time) {
        this.time = time;
    }
    public void setX(float x) {
        this.x = x;
    }
    public void setY(float y) {
        this.y = y;
    }
    public void setRotationSpeed(float rotationSpeed) {
        this.rotationSpeed = rotationSpeed;
    }
    public void setSize(float width, float height){
        this.sprite.setSize(width, height);
    }
    public void setColor(Color color){
        sprite.setColor(color);
    }
}
