package com.aa.ld27.particle;

/**
 * A Particle. (Something that looks pretty)
 * <p/>
 * @author Toni
 */
public interface Particle {
    //Methoden
    /**
     * That what should happen every frame.
     */
    public void act();
    /**
     * Renders the Particle.
     */
    public void render();
    /**
     * If the Particle is still active.
     * @return False if the Particle can be removed.
     */
    public boolean isActive();
}