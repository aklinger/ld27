package com.aa.ld27.particle;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Manages a lot of Particles (stuff that only looks pretty) with an ArrayList.
 * <p/>
 * It maybe doesn't do this the best way, but if it's a bottleneck, you can
 * write your own implementation.
 * <p/>
 * @author Toni
 */
public class ParticleList implements ParticleManager {
    //Attribute
    private ArrayList<Particle> particles;
    //Konstruktor
    /**
     * Creates a new ParticleList using a simple ArrayList.
     */
    public ParticleList() {
        particles = new ArrayList<Particle>();
    }
    //Methoden
    @Override
    public void addParticle(Particle particle) {
        particles.add(particle);
    }
    @Override
    public void addAllParticles(Collection<Particle> particles) {
        this.particles.addAll(particles);
    }
    @Override
    public void act() {
        List<Particle> trash = new ArrayList<Particle>();
        for (Particle part : particles) {
            part.act();
            if (!part.isActive()) {
                trash.add(part);
            }
        }
        particles.removeAll(trash);
        particles.trimToSize();
    }
    @Override
    public void render() {
        for (Particle part : particles) {
            part.render();
        }
    }
}
