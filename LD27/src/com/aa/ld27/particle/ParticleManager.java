package com.aa.ld27.particle;

import java.util.Collection;

/**
 * An abstract representation of a Class that manages Particles. (Stuff that can
 * only look pretty)
 * <p/>
 * The implementation that is the fastest in the specific needed case should
 * then be chosen (or make it yourself).
 * <p/>
 * @author Toni
 */
public interface ParticleManager {
    /**
     * Adds the Particle to the Manager
     * <p/>
     * @param particle The Particle to add.
     */
    public void addParticle(Particle particle);
    /**
     * May add all Particles in the Collection to the Manager
     * <p/>
     * @param particles The Particles to add to the Manager.
     */
    public void addAllParticles(Collection<Particle> particles);
    /**
     * Is called once every frame. Updates all the Particles and manages them.
     */
    public void act();
    /**
     * Renders all the Particles.
     */
    public void render();
}
