package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.Collision;
import com.aa.ld27.maths.HitBox;

/**
 * FUN
 * 
 * @author Toni
 */
public class Atom extends Entity{
    //Attributes
    private static final float GRAVITY_CONSTANT = 5.05f;
    
    
    //Constructors
    public Atom() {
        setHostile(false);
        setMaxHealth(10);
        //setFx(0.95f);
        //setFy(0.95f);
        //setGy(-0.3f);
        float wtf = (float)Math.random();
        setMass(10+wtf*100);
        setFanta(Entity.loadSingleImage("staub2"));
        float size = 2+30*wtf;
    	setDrawableSize(size, size, true);
        setSolid(true);
    }
    
    
    //Methods
    @Override
    public void act(EntityManager man) {
        computeMovement();
        getFanta().act(1);
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        super.interactWith(ent2, col, man);
        float dx = ent2.getX()-this.getX();
        float dy = ent2.getY()-this.getY();
        double dist_squared = dx*dx+dy*dy;
        float factor = (float)((GRAVITY_CONSTANT*this.getMass()*ent2.getMass())/(dist_squared));
        if(Float.isInfinite(factor) || Float.isNaN(factor)){
            factor = 0.0001f;
        }
        //System.out.println("factor: "+factor);
        dist_squared = Math.sqrt(dist_squared);
        dx /= Math.sqrt(dist_squared);
        dy /= Math.sqrt(dist_squared);
        dx = dx * factor;
        dy = dy * factor;
        addSpeed(dx / getMass(), dy / getMass());
        ent2.addSpeed(-dx / ent2.getMass(), -dy / ent2.getMass());
    }
    
    
    
    
    //Getter & Setter
    @Override
    public int getType() {
        return HitBox.CIRCLE;
    }
    
}
