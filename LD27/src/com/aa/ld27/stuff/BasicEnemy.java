package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.Collision;
import com.aa.ld27.util.LogUtil;

/**
 * A basic Enemy.
 * 
 * @author Toni
 */
public class BasicEnemy extends Entity{
    //Attributes
    private float moveSpeed;
    
    //Constructors
    public BasicEnemy(){
        setHostile(true);
        setMaxHealth(8);
        setFx(0.95f);
        setFy(0.95f);
        setGy(-0.3f);
        setMass(1.5f);
        setFanta(Entity.loadSingleAnimation("enemy_ground",30));
    	setDrawableSize(32, 32, true);
    	
        moveSpeed = 0.03f;
    }
    //Methods
    @Override
    public void act(EntityManager man) {
        getFanta().act(1);
    	addSpeed(-moveSpeed, 0);
        computeMovement();
        if(getX()<0){
            setMaxHealth(0);
            man.damageCastle(1);
            LogUtil.log("AN ENEMY ESCAPED");
        }
    }

    //Getter & Setter
    public void setMoveSpeed(float moveSpeed) {
        this.moveSpeed = moveSpeed;
    }
}
