package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.badlogic.gdx.graphics.g2d.Animation;

/**
 * The Destruction-Animation of the Castle.
 * 
 * @author Toni
 */
public class BreakingCastle extends Entity{
    //Attributes
    private boolean breaking;
    
    //Constructors
    public BreakingCastle(float x, float y, float width, float height) {
        Fanta fanta = Entity.loadSingleAnimation("Turm_Animation/Tower",3);
        setFanta(fanta);
        fanta.getAnimation(fanta.getCurrentAnimation()).setPlayMode(Animation.NORMAL);
        setDrawableSize(width, height, true);
    	
    	setFixated(true);
        setSolid(false);
        
        setX(x);
        setY(y);
        System.out.println("FANTEAS");
        System.out.println("fan: "+fanta.getAnimation(fanta.getCurrentAnimation()).getKeyFrameIndex(12));
    }
    
    //Methods
    @Override
    public void act(EntityManager man) {
        if(breaking){
            getFanta().act(1);
        }
    }

    //Getter & Setter
    public void setBreaking(boolean breaking) {
        this.breaking = breaking;
    }

    @Override
    public boolean isForeground() {
        return !breaking;
    }

    @Override
    public boolean isAlive() {
        return true;
    }
}
