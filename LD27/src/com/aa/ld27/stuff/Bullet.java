package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.Collision;

/**
 * A Bullet from an enemy.
 * 
 * @author Toni
 */
public class Bullet extends Entity{
    //Attributes
    private int damage;
    private int maxTime;
    private int time;
    
    public static final String NORMAL_BULLET = "powerup";
    public static final String FIRE_BULLET = "fire";
    
    //Constructors
    public Bullet(float posx, float posy, float direction, float moveSpeed, String type) {
        setFanta(Entity.loadSingleImage(type));
        switch(type){
            case NORMAL_BULLET:
                setDrawableSize(16, 16, true);
                break;
            case FIRE_BULLET:
                setDrawableSize(32, 32, true);
                break;
            default:
                setDrawableSize(16, 16, true);
                break;
        }
        
        damage = 1;
        maxTime = 100;
        setX(posx);
        setY(posy);
        setSx((float)(Math.sin(direction) * moveSpeed));
        setSy((float)(Math.cos(direction) * moveSpeed));
        setSolid(false);
    }
    
    //Methods
    @Override
    public void act(EntityManager man) {
        computeMovement();
        rotatoToDirectionMoving(0);
        time++;
        if(time>maxTime){
        	this.setMaxHealth(0);
        }
    }
    
    //Getter & Setter
    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        if(col != null){
            if(ent2 instanceof GoatEntity){
                ent2.damage(damage);
                this.setMaxHealth(0);
            }
        }
    }
    
}
