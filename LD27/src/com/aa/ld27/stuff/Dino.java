package com.aa.ld27.stuff;

import com.aa.ld27.dal.SoundManager;
import com.aa.ld27.logic.EntityManager;
import com.badlogic.gdx.math.Rectangle;
import java.util.List;

/**
 * The large Dino Boss that spits fire.
 * 
 * @author Toni
 */
public class Dino extends BasicEnemy{
    //Attributes
    private float shootMaxDir;
    private float shootDir;
    private float shootDirDelta;
    private float shootDeltaSpeed;
    
    private float shotTime;
    private float nextShot;
    private float shootDelay;
    private float shotsInInterval;
    private float shotsFired;
    private float shotCooldown;
    
    //Constructors
    public Dino() {
        setFx(0.95f);
        setFy(0.95f);
        setGy(-0.5f);
        setMass(10.5f);
        setMaxHealth(100);
        
        setFanta(Entity.loadSingleImage("dino"));
    	setDrawableSize((64*2), (64*2), false);
        setHitBox(new Rectangle(2, 0, (64*2)-20, (64*2)-20));
    	
        setMoveSpeed(0.01f);
        shootMaxDir = (float)Math.toRadians(88);
        shootDir = 0;
        shootDirDelta = 0.1f;
        shootDeltaSpeed = 4;
        
        shotTime = 0;
        nextShot = 0;
        shootDelay = 5;
        shotsInInterval = 20;
        shotCooldown = 60*10;
        
        SoundManager.getInstance().play(SoundManager.DRAGON_ROAR);
        //enableHardMode();
    }
    public void enableHardMode(){
        shootMaxDir = (float)Math.toRadians(90);
        shootDir = 0;
        shootDirDelta = 0.2f;
        shootDeltaSpeed = 5;
        
        shotTime = 0;
        nextShot = 0;
        shootDelay = 1;
        shotsInInterval = 200;
        shotCooldown = 60*2;
    }
    //Methods
    @Override
    public void act(EntityManager man) {
        super.act(man);
        acquireTarget(man);
        if(getX() < 170){
            setX(170);
        }
        shotTime++;
        shootDir += shootDirDelta;
        if(shotTime > nextShot){
            if(shotsFired == 0){
                SoundManager.getInstance().play(SoundManager.DRAGON_FIRE);
            }
            shotTime = 0;
            shotsFired++;
            if(shotsFired>shotsInInterval){
                nextShot = (int)(Math.random()*shotCooldown*0.5+shotCooldown*0.5);
                shotsFired = 0;
            }else{
                nextShot = (int)(shootDelay);
            }
            float extraDir = isFlipX() ? 80 : 360-80;
            float extra = isFlipX() ? 64 : -64;
            float shootDirection = (float)(Math.toRadians(extraDir)+Math.sin(shootDir)*shootMaxDir);
            man.addEntity(new Bullet(getCenter().x+extra,getCenter().y-24,shootDirection,shootDeltaSpeed,Bullet.FIRE_BULLET));
        }
    }
    
    private void acquireTarget(EntityManager man){
        List<Entity> players = man.getAliveTargets();
        if(players.size()>0){
            Entity target = null;
            float maxDistToTheRight = 0;
            for (Entity thing : players) {
                float dist = thing.getCenter().x;
                if(dist > maxDistToTheRight){
                    maxDistToTheRight = dist;
                    target = thing;
                }
            }
            if(target != null){
                if(target.getCenter().x > this.getCenter().x){
                    setMoveSpeed(-0.015f);
                    setFlipX(true);
                }else{
                    setMoveSpeed(0.01f);
                    setFlipX(false);
                }
            }
        }
    }
    
    //Getter & Setter
    @Override
    public void die(EntityManager man) {
        SoundManager.getInstance().play(SoundManager.DRAGON_ROAR);
    }
}
