package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.Collision;
import com.aa.ld27.util.LogUtil;

/**
 * The Door of the tower.
 * 
 * @author Toni
 */
public class Door extends Fancy{
    //Attributes
    private float teleX;
    private float teleY;
    
    //Constructors
    public Door(float x, float y, float width, float height, String atlasIndex) {
        super(x, y, width, height, atlasIndex, false);
        //teleX = -108;
        //teleY = 300;
        teleX = 30;
        teleY = 468;
    }
    
    
    //Getter & Setter

    //Methods
    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        super.interactWith(ent2, col, man);
        if(col != null){
            if(ent2 instanceof GoatEntity){
                //ent2.addPosition(teleX,teleY);
            	ent2.setX(teleX);
            	ent2.setY(teleY);
            	ent2.setSpawnProtection(true);
            }
        }
    }
    @Override
    public void render(EntityManager man) {
    	if(LogUtil.isDebug()){
            super.render(man);
    	}
    }
    
}
