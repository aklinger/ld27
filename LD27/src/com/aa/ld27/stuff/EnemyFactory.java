package com.aa.ld27.stuff;

/**
 * Creates the enemies.
 * 
 * @author Toni
 */
public class EnemyFactory {
    //Attributes
    public static final int NORMAL = 1;
    public static final int SHIELD = 2;
    public static final int FLYING = 3;
    public static final int DRAGON = 4;
    public static final int FlYING_SHIP = 5;
    public static final int SPIKEY = 6;
    public static final int CATAPULT = 7;
    public static final int FROG = 7;
    
    //Constructors
    public static Entity createEnemy(int type){
        Entity ent = null;
        switch(type){
            case NORMAL:
                ent = new BasicEnemy();
                break;
            case FLYING:
                ent = new FlyingEnemy();
                break;
            case FROG:
                ent = new FrogEnemy();
                break;
            case DRAGON:
                ent = new Dino();
                break;
        }
        return ent;
    }
    //Methods
    
    //Getter & Setter
    
}
