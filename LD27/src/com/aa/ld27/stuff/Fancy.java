package com.aa.ld27.stuff;

import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.logic.EntityManager;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;

/**
 * Generic class for displaying background images.
 * 
 * @author Toni
 */
public class Fancy extends Entity{
    //Attributes
    private boolean foreground;
    //Constructors
    public Fancy(float x, float y, float width, float height, String atlasIndex, boolean foreground){
    	/*TextureAtlas.AtlasRegion region = AbstractScreen.getAtlas().findRegion(atlasIndex);
        Sprite sprite = new Sprite(region);
    	setSprite(sprite,true);
    	setDrawableSize(width, height, true);*/
        
        setFanta(Entity.loadSingleImage(atlasIndex));
        setDrawableSize(width, height, true);
    	
    	setFixated(true);
        setSolid(false);
        
        setX(x);
        setY(y);
        this.foreground = foreground;
    }

    //Methods
    @Override
    public void act(EntityManager man) {
        //Do nothing yet, later animation
    }
    //Getter & Setter
    
    @Override
    public boolean isForeground() {
        return foreground;
    }
}
