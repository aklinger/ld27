package com.aa.ld27.stuff;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import java.util.HashMap;

/**
 * A sprite that manages Animations.
 * <p/>
 * @author Toni
 */
public class Fanta {
    //Attributes
    private String currentAnimation;
    private HashMap<String, Animation> animations;
    private float time;

    //Constructors
    public Fanta(String currentAnimation) {
        this.currentAnimation = currentAnimation;
        
        animations = new HashMap<String, Animation>();
        time = 0;
    }
    
    //Methods
    public void act(float delta) {
        time += delta;
    }

    public TextureRegion getCurrentFrame() {
        return animations.get(currentAnimation).getKeyFrame(time);
    }

    public void putTextureRegion(String name, TextureRegion region){
        Animation anim = new Animation(1, region);
        putAnimation(name,anim);
    }
    
    public void putAnimation(String name, Animation anim) {
        animations.put(name, anim);
    }

    //Getter & Setter
    public void setCurrentAnimation(String currentAnimation) {
        this.currentAnimation = currentAnimation;
    }

    public String getCurrentAnimation() {
        return currentAnimation;
    }
    
    public void setTime(float time) {
        this.time = time;
    }

    public Animation getAnimation(String name) {
        return animations.get(name);
    }
}
