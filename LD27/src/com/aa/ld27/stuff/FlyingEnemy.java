package com.aa.ld27.stuff;

import java.util.List;
import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.CollisionUtil;
import com.badlogic.gdx.math.Vector2;

/**
 * A basic Flying enemy.
 * 
 * @author Toni
 */
public class FlyingEnemy extends BasicEnemy{
    //Attributes
    private int fireRate;
    private int nextShot;
    private int shotTime;
    private float upAndDownMovement;
    private float upAndDownMovementDelta;
    private float amplitude;
    
    //Constructors
    public FlyingEnemy() {
        setGy(0);
        setMass(4);
        setMaxHealth(5);
        
        fireRate = (int)(600*1.5f);
        nextShot = (int)(fireRate*Math.random());
        upAndDownMovementDelta = 0.05f+(float)Math.random()*0.05f;
        amplitude = 0.05f+(float)Math.random()*0.05f;
        
        setFanta(Entity.loadSingleAnimation("enemy_air",30));
    }
    
    //Methods
    @Override
    public void act(EntityManager man) {
        super.act(man);
        shotTime++;
        if(shotTime > nextShot){
            shotTime = 0;
            nextShot = (int)(fireRate*Math.random());
            List<Entity> players = man.getAliveTargets();
            if(players.size()>0){
                Entity target = null;
                float minDist = Float.MAX_VALUE;
                for (Entity thing : players) {
                    float dist = thing.getCenter().dst2(this.getCenter());
                    if(dist < minDist){
                        minDist = dist;
                        target = thing;
                    }
                }
                if(target != null){
                    Vector2 playerPos = target.getCenter();
                    man.addEntity(new Bullet(getCenter().x,getCenter().y,CollisionUtil.directionTo(this.getCenter(), playerPos),5,Bullet.NORMAL_BULLET));
                }
            }
        }
        upAndDownMovement += upAndDownMovementDelta;
        addSpeed(0, (float)Math.sin(upAndDownMovement)*amplitude);
    }
    
    
    
    //Getter & Setter

    @Override
    public void die(EntityManager manager) {
        //TODO maybe better düsenantrieb for goat (left and right movement)
        //TODO make standing on moving platform also move object on platform
        //TODO find bullet picture for flying enemy
        //TODO create attack for melee unit
    }

    
}
