package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;

/**
 * Frog that jumps around.
 * 
 * @author Toni
 */
public class FrogEnemy extends BasicEnemy{
    //Attributes
    private float jumpYSpeed;
    private float jumpXSpeed;
    private float timeSinceLastJump;
    private float jumpMinDelay;
    private float jumpMaxDelay;

    //Constructors
    public FrogEnemy() {
        setFanta(loadSingleAnimation("enemy_jump",30));
    	setDrawableSize(32, 32, true);
        setMaxHealth(12);
        setMoveSpeed(0.005f);
        
        jumpYSpeed = 15;
        jumpXSpeed = 5;
        jumpMinDelay = 1*60;
        jumpMaxDelay = 5*60;
    }
    
    
    //Methods
    @Override
    public void act(EntityManager man) {
        super.act(man);
        timeSinceLastJump++;
        if((timeSinceLastJump*Math.random()*0.5f)+timeSinceLastJump-jumpMinDelay > jumpMaxDelay){
        	timeSinceLastJump = 0;
        	setSx(-jumpXSpeed);
        	setSy(jumpYSpeed);
        }
    }
    //Getter & Setter
    
}
