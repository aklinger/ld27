package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.Collision;
import com.aa.ld27.dal.SoundManager;
import com.aa.ld27.dal.control.GoatControlScheme;
import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.particle.LightWeightParticle;
import com.aa.ld27.util.ColorUtil;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureAtlas.AtlasRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

/**
 * Simply the player controlled object.
 * <p/>
 * 
 * @author Toni
 */
public class GoatEntity extends Entity {
	// Attributes
	private float walkAccel;
	private float runAccel;
	private float dashAccel;
	private float smashSpeed;
	private float jumpSpeed;
	private GoatControlScheme steuerung;
	private boolean smashing;
	private boolean touchedGround;

	private Color color;

	private int blockAnimation;
	private int maxMeh = 60;
	private int mehTime;

	private int powerupTime;
	private float powerIncrease;
	private float flyAccel;

	// Constructors
	public GoatEntity(GoatControlScheme steuerung) {
		this.steuerung = steuerung;
		setFx(0.94f);
		setFy(0.94f);
		setGy(-0.5f);
		setMaxHealth(10 * 2);
		walkAccel = 0.45f;
		runAccel = 0.8f;
		dashAccel = 5.81f;
		smashSpeed = 15f;
		jumpSpeed = 18f;

		powerIncrease = 1.3f;
		flyAccel = 0.93f;

		Fanta fanta = new Fanta("walking");
		Array<AtlasRegion> regions = AbstractScreen.getAtlas().findRegions(
				"goat_small");
		fanta.putTextureRegion("default", regions.get(0));
		fanta.putAnimation("walking",
				new Animation(10, regions.get(0), regions.get(1)));
		fanta.getAnimation("walking").setPlayMode(Animation.LOOP);
		fanta.putAnimation("dashing", new Animation(10, regions.get(2)));
		fanta.putAnimation("smashing", new Animation(10, regions.get(3)));
		fanta.putAnimation("eating", new Animation(10, regions.get(4)));
		fanta.putAnimation("goating", new Animation(10, regions.get(5)));
		setFanta(fanta);

		setDrawableSize(64, 64, false);
		setHitBox(new Rectangle(5, 0, 64 - 5 * 2, 42));
		color = ColorUtil.randomGoatColor();
	}

	// Methods
	@Override
	public void act(EntityManager man) {
		steuerung.update();
		blockAnimation--;
		getFanta().act(1);
		boolean moved = false;
		if (steuerung.getLeft().isDown()) {
			moved = true;
			setFlipX(false);
			if (steuerung.getRun().isPressed()) {
				addSpeed(-dashAccel * getPowerupFactor(), 0);
				// animTime = animRate*2.1f;
				getFanta().setCurrentAnimation("dashing");
				blockAnimation = 10;
				LightWeightParticle.particleExplosion(man, 20,
						getCenter().x - 10, this.getY(), 8, Math.toRadians(45),
						Math.toRadians(45), 1f, 4f, 0.01f, "staub2", 30, true,
						1f, 16);
			} else if (steuerung.getRun().isDown()) {
				addSpeed(-runAccel * getPowerupFactor(), 0);
			} else {
				addSpeed(-walkAccel * getPowerupFactor(), 0);
			}
		}
		if (steuerung.getRight().isDown()) {
			moved = true;
			setFlipX(true);
			if (steuerung.getRun().isPressed()) {
				addSpeed(dashAccel * getPowerupFactor(), 0);
				// animTime = animRate*2.1f;
				getFanta().setCurrentAnimation("dashing");
				blockAnimation = 10;
				LightWeightParticle.particleExplosion(man, 20, getCenter().x,
						this.getY(), 8, Math.toRadians(360 - 45),
						Math.toRadians(45), 1f, 4f, 0.01f, "staub2", 30, true,
						1f, 16);
			} else if (steuerung.getRun().isDown()) {
				addSpeed(runAccel * getPowerupFactor(), 0);
			} else {
				addSpeed(walkAccel * getPowerupFactor(), 0);
			}
		}
		if (steuerung.getSmash().isPressed() && !touchedGround && !isPowerup()) {
			setSx(0);
			setSy(-smashSpeed);
			smashing = true;
		}
		if (isPowerup()) {
			if (steuerung.getUp().isDown()) {
				addSpeed(0, flyAccel);
			}
			if (steuerung.getDown().isDown()) {
				addSpeed(0, -flyAccel);
			}
		}
		if (steuerung.getShit().isDown()) {
			float extra = !isFlipX() ? 23 : -23 - 2;
			float speed = !isFlipX() ? (float) (Math.random() * 0.8f)
					: (float) (-Math.random() * 0.8f);
			LightWeightParticle lwp = LightWeightParticle.createStaticParticle(
					man, getCenter().x + extra, getCenter().y + 10, 2,
					"base_ground", 30, false, 2);
			lwp.setGy(-0.1f);
			lwp.setSx(speed);
		}
		mehTime--;
		if (steuerung.getMeh().isPressed() && mehTime < 0) {
			moved = true;
			SoundManager.getInstance().play(SoundManager.MEH);
			// animTime = animRate*5.1f;
			getFanta().setCurrentAnimation("goating");
			mehTime = maxMeh;
			blockAnimation = 10;
		}
		if (steuerung.getEat().isPressed()) {
			if (man.eatGrass(this)) {
				moved = true;
				SoundManager.getInstance().play(SoundManager.EAT1,
						SoundManager.EAT2);
				// animTime = animRate*4.1f;
				getFanta().setCurrentAnimation("eating");
				blockAnimation = 10;
				this.heal(1);
				this.setSx(0);
			}
		}
		computeMovement();
		if (getX() < 0) {
			setX(0);
		} else if (getX() > 1536 - 64) {
			setX(1536 - 64);
		}
		if (getY() < 0) {
			setY(600);
		}

		if (isPowerup()) {
			setGy(0);
			powerupTime--;
			// Duesenantrieb:
			float extra = isFlipX() ? -7 : 0;
			LightWeightParticle.particleExplosion(man, 4, getCenter().x - 15
					+ extra, this.getY(), 5, Math.toRadians(180),
					Math.toRadians(45), 0.3f, 1.5f, 0.01f, "staub2", 25, true,
					5f, 8);
			LightWeightParticle.particleExplosion(man, 4, getCenter().x + 15
					+ extra, this.getY(), 5, Math.toRadians(180),
					Math.toRadians(45), 0.3f, 1.5f, 0.01f, "staub2", 25, true,
					5f, 8);
		} else {
			setGy(-0.5f);
		}

		if (blockAnimation <= 0) {
			getFanta().setCurrentAnimation("walking");
			if (!moved || isPowerup()) {
				getFanta().setTime(0);
			}
		}
		touchedGround = false;
	}

	public void addPowerUpTime(int time) {
		powerupTime += time;
	}

	private float getPowerupFactor() {
		if (isPowerup()) {
			return powerIncrease;
		} else {
			return 1f;
		}
	}

	private boolean isPowerup() {
		return powerupTime > 0;
	}

	@Override
	protected void drawTheThing() {
		if (smashing) {
			// animTime = animRate*3.5f;
			getFanta().setCurrentAnimation("smashing");
		}
		if (isPowerup()) {
			this.setColor(ColorUtil.randomColor(0.3f, 0.3f, 0.3f));
		} else {
			this.setColor(color);
		}
		super.drawTheThing();
	}

	@Override
	public void damage(float damage) {
		if (isPowerup()) {
			damage /= 2;
		}
		super.damage(damage);
	}

	@Override
	public void interactWith(Entity ent2, Collision col, EntityManager man) {
		if (col != null && ent2.isSolid()) {
			if (ent2 instanceof BasicEnemy) {
				float speedDamageTreshold = 7;
				if (isPowerup()) {
					Vector2 velocity = new Vector2(getSx(), getSy());
					float speed = velocity.len();
					float impactFactor = velocity
							.cpy()
							.nor()
							.dot(col.getDisplacementRelativeTo(ent2).cpy()
									.nor());
					if (speed > speedDamageTreshold && impactFactor > 0.25) {
						ent2.damage((speed / 2) * getPowerupFactor()
								* impactFactor);
						SoundManager.getInstance().play(SoundManager.HIT2);
						Vector2 diff = ent2.getCenter().sub(this.getCenter());
						LightWeightParticle.createStaticParticle(man,
								getCenter().x + diff.x / 2, getCenter().y
										+ diff.y / 2, 10, "pow", 60, false, 48);
					}
				} else if (col.getDisplacementRelativeTo(this).x != 0) {
					if (Math.abs(getSx()) > speedDamageTreshold) {
						ent2.damage(Math.abs(getSx() / 2));
						SoundManager.getInstance().play(SoundManager.HIT2);
						float extra = getSx() > 0 ? 32 : -32 - 48;
						LightWeightParticle.createStaticParticle(man,
								getCenter().x + extra, getCenter().y - 10, 10,
								"pow", 60, false, 48);
					}
				}
				if (smashing) {
					ent2.damage(5);
					SoundManager.getInstance().play(SoundManager.HIT1);
					LightWeightParticle.createStaticParticle(man,
							getCenter().x, getY(), 32, "pow", 60, false, 48);
				}
			}
			if (col.getDisplacementRelativeTo(this).x == 0
					&& col.getDisplacementRelativeTo(this).y > 0) {
				touchedGround = true;
				if (smashing) {
					smashing = false;
					SoundManager.getInstance().play(SoundManager.HIT1);
				} else if (getSy() < -2.5f) {
					SoundManager.getInstance().play(SoundManager.LESSER_HIT);
				}
				if (steuerung.getJump().isDown()) {
					setSy(jumpSpeed);
					SoundManager.getInstance().play(SoundManager.JUMP);
					LightWeightParticle.particleExplosion(man, 20,
							getCenter().x, getY(), 2, Math.PI,
							Math.toRadians(180), 0.2f, 2, 0, "staub2", 30,
							true, 0.1f, 16);
				} else {
					setSy(0);
				}
			}
		}
	}
}
