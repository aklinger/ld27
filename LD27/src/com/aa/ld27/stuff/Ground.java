package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.Collision;
import com.aa.ld27.util.LogUtil;

public class Ground extends Entity{
    
    private boolean castlePart;
    
    public Ground(float width, float height, boolean castlePart){
        setFanta(Entity.loadSingleImage("base_ground"));
    	setDrawableSize(width, height, true);
    	
    	setFixated(true);
        this.castlePart = castlePart;
    }

    @Override
    public void act(EntityManager man) {
        //Do nothing, I'm just the ground
    }
    @Override
    public void render(EntityManager man) {
    	if(LogUtil.isDebug()){
    		super.render(man);
    	}
    }

    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        if(col != null && castlePart){
            if(ent2 instanceof Bullet || ent2 instanceof BasicEnemy){
                man.damageCastle(1);
                ent2.setMaxHealth(0);
            }
        }
    }
}
