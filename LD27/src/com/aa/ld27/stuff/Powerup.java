package com.aa.ld27.stuff;

import com.aa.ld27.gui.AbstractScreen;
import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.maths.Collision;
import com.aa.ld27.util.ColorUtil;

/**
 * The Powerup that makes the goat fly.
 * 
 * @author Toni
 */
public class Powerup extends Entity{
    
    //Attributes
    private int powerupDuration = 10*60;
    
    //Constructors
    public Powerup(float x, float y) {
        setX(x);
        setY(y);
        
        setFx(0.995f);
        setFy(0.995f);
        setGy(-0.1f);
        setFanta(Entity.loadSingleImage("cabbage"));
    	setDrawableSize(32, 32, true);
    }
    
    //Methods
    @Override
    public void act(EntityManager man) {
        computeMovement();
        if(getX()>1500){
            setX(1500);
        }
    }
    
    @Override
    public void interactWith(Entity ent2, Collision col, EntityManager man) {
        super.interactWith(ent2, col, man);
        if(col != null){
            if(ent2 instanceof GoatEntity){
                ((GoatEntity)ent2).addPowerUpTime(powerupDuration);
                this.setMaxHealth(0);
            }
        }
    }

    @Override
    public boolean collidesWith(Entity other) {
        if(other instanceof BasicEnemy || other instanceof PowerupShittingPig){
            return false;
        }else{
            return super.collidesWith(other);
        }
    }

    @Override
    protected void drawTheThing() {
        this.setColor(ColorUtil.randomColor(0.5f,0.5f,0.5f));
        super.drawTheThing();
    }
}
