package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.aa.ld27.particle.LightWeightParticle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.math.Rectangle;
import java.util.Collection;

/**
 * The flying pig that shits powerups.
 * 
 * @author Toni
 */
public class PowerupShittingPig extends Entity{
    //Attributes
    private int powerupTime;
    private int time;
    private float moveSpeed;
    private int minX;
    private int maxX;
    
    private float upAndDownMovement;
    private float upAndDownMovementDelta;
    private float amplitude;
    
    
    //Constructors
    public PowerupShittingPig() {
        powerupTime = 60*30;
        time = 60*20;
        
        updateMoveSpeed();
        minX = 50;
        maxX = 1400;
        
        setX(minX);
        setY(2000);
        setSolid(true);
        setFanta(Entity.loadSingleImage("nyan_goat"));
        float size = 64*2;
        setDrawableSize(size, size, false);
        setHitBox(new Rectangle(30, 4, size-30*2, size-30*2));
        
        upAndDownMovementDelta = 0.015f;
        amplitude = 1f;
    }
    
    private void updateMoveSpeed(){
        moveSpeed = (float)(1+Math.random());
    }
    
    //Methods
    @Override
    public void act(EntityManager man) {
        if(isFlipX()){
            addPosition(-moveSpeed, 0);
        }else{
            addPosition(moveSpeed, 0);
        }
        if(getX() < minX){
            setX(minX);
            setFlipX(!isFlipX());
            updateMoveSpeed();
        }
        if(getX() > maxX){
            setX(maxX);
            setFlipX(!isFlipX());
            updateMoveSpeed();
        }
        time++;
        if(time > powerupTime)
        {
            time = 0;
            Powerup p = new Powerup(getX(), getY());
            man.addEntity(p);
        }
        upAndDownMovement += upAndDownMovementDelta;
        addPosition(0, (float)Math.sin(upAndDownMovement)*amplitude);
        
        Color[] rainbow = new Color[]{Color.RED,Color.ORANGE,Color.YELLOW,Color.GREEN,Color.CYAN,Color.BLUE,Color.MAGENTA};
        float extra = isFlipX() ? 23 : -23-2;
        float flip = isFlipX() ? 1 : -1;
        float rainbowHeight = getImgH()*0.5f;
        float rainbowPart = rainbowHeight/rainbow.length;
        for (int i = 0; i < rainbow.length; i++) {
            Collection<LightWeightParticle> particleExplosion = LightWeightParticle.particleExplosion(man, 2, getCenter().x+extra, this.getY()+rainbowHeight-rainbowPart*i, 8, Math.toRadians(90 * flip), Math.toRadians(45), 1f, 2f, 0f, "staub2", 60*2, true, 1f, 8);
            tintParticles(particleExplosion, rainbow[i]);
        }
    }
    
    private void tintParticles(Collection<LightWeightParticle> particles, Color color){
        for(LightWeightParticle p : particles){
            p.setColor(color);
        }
    }

    //Getter & Setter
    @Override
    public boolean isForeground() {
        return true;
    }
}
