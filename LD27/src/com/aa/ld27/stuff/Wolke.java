package com.aa.ld27.stuff;

import com.aa.ld27.logic.EntityManager;
import com.badlogic.gdx.math.Rectangle;

/**
 * 
 * 
 * @author Toni
 */
public class Wolke extends Entity{
    //Attributes
    private float speedx;

    //Constructors
    public Wolke(float x, float y) {
        setFanta(Entity.loadSingleImage("wolke"));
        setDrawableSize(64, 64, false);
        setHitBox(new Rectangle(2, 2, 52, 16));
        setX(x);
        setY(y);
        setFixated(true);
        
        speedx = (float)Math.random()*0.5f+0.1f;
    }
    
    //Methods
    @Override
    public void act(EntityManager man) {
        addPosition(speedx, 0);
        if(getX()>1536){
            setX(-55);
            speedx = (float)Math.random()*0.5f+0.1f;
        }
    }

    //Getter & Setter
    @Override
    public boolean isForeground() {
        return true;
    }
}

