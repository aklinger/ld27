/*
 * To change this default template go to 
 * Tools|Templates|Licenses|Default License
 */

package com.aa.ld27.util;

import com.badlogic.gdx.graphics.Color;

/**
 *
 * @author Toni
 */
public class ColorUtil {
    //Attributes
    
    //Constructors
    
    //Methods
    public static Color randomGoatColor(){
        return new Color(0.8f+(float)(Math.random()*0.2f), 0.5f+(float)(Math.random()*0.5f), 0.5f+(float)(Math.random()*0.5f), 1f);
    }
    public static Color randomColor(float minR, float minG, float minB){
        return new Color(minR+(float)(Math.random()*(1-minR)), minG+(float)(Math.random()*(1-minG)), minB+(float)(Math.random()*(1-minB)), 1f);
    }
    
    //Getter & Setter
    
}
