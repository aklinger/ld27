package com.aa.ld27.util;

import com.badlogic.gdx.tools.imagepacker.TexturePacker2;


/**
 * Packs all the individual images into one big image and creates a texture-atlas to find all the images again.
 * 
 * http://code.google.com/p/libgdx/wiki/TexturePacker
 * 
 * @author Toni
 */
public class MyTexturePacker {
    
    private static final String INPUT_DIR = "../ld27-android/assets/gfx";
    private static final String OUTPUT_DIR = "../ld27-android/assets/image-atlases";
    private static final String PACK_FILE = "pages-info";

    public static void main(
        String[] args )
    {
        // pack the images
        TexturePacker2.process(INPUT_DIR, OUTPUT_DIR, PACK_FILE );
    }
}
